//License : Please see Rarium.LICENSE
#include <Rarium/Core/System.h>

#include <iostream>
#include <string>

using namespace std;

int main(int argc, char* args[])
{
	Rarium::System system;

	system.Run();

	return 0;
}