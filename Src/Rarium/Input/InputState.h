//License : Please see Rarium.LICENSE
#ifndef _RariumInputState_

#define _RariumInputState_

#include <glm/glm.hpp>
#include <SDL2/SDL.h>
#include <Rarium/Core/Rarium.h>
#include <string>

namespace Rarium
{
	/**
	* State of user input
	*/
	class Rarium_API InputState
	{
	public:
		/// Build the input state
		InputState();
		/// Update the input state
		bool Update();

		/// Get the position of the mouse on screen
		inline glm::vec2 GetMousePosition() { return _mousePosition; }
		/// Get if yes or no the left mouse button is pressed
		inline bool IsLeftButtonDown() { return _leftButtonDown; }
		/// Get if yes or no the right mouse button is pressed
		inline bool IsRightButtonDown() { return _rightButtonDown; }
		/// Get the amount the scroll the middle mouse wheel
		inline int GetWheelAmount() { return _wheelAmount; }
		/// Set the size of the display window
		inline void SetWindowSize(glm::vec2 pSize) { _windowSize = pSize; }

		/// Update a string if there is text event
		std::string UpdateText(std::string pText);

	protected:
		/**
		*	Handle a mouse button down event
		*	@param e The mouse button down event
		*/
		void HandleMouseButtonDown(SDL_Event e);
		/**
		*	Handle a mouse button up event
		*	@param e The mouse button up event
		*/
		void HandleMouseButtonUp(SDL_Event e);
		/**
		*	Handle a mouse wheel event
		*	@param e The mouse button up event
		*/
		void HandleMouseWheel(SDL_Event e);
		/**
		*	Handle a text event
		*	@param e The text event
		*/
		void HandleText(SDL_Event e);
		/**
		*	Handle a keyboard event
		*	@param e The keyboard event
		*/
		void HandleKeyboard(SDL_Event e);

		glm::vec2 _mousePosition;///< Mouse position on screen
		glm::vec2 _windowSize;///< Size of the display window
		int _wheelAmount;///< Middle scroll wheel amount
		bool _leftButtonDown;///< If yes or no the left mouse button is pressed
		bool _rightButtonDown;///< If yes or no the right mouse button is pressed

		std::string _text;///< Text from text event
		bool _backspacePressed;///< If yes of no the backspace key is pressed
	};
}

#endif