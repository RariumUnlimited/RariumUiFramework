//License : Please see Rarium.LICENSE
#include <Rarium/Input/InputState.h>

#include <Rarium/Core/Log.h>

namespace Rarium
{
	InputState::InputState()
	{
		_leftButtonDown = false;
		_rightButtonDown = false;
		_mousePosition = glm::vec2(0.0, 0.0);
		_wheelAmount = 0;
	}

	bool InputState::Update()
	{
		bool result = false;
		_backspacePressed = false;
		_text = "";

		_wheelAmount = 0;

		SDL_Event e;
		while (SDL_PollEvent(&e) != 0)
		{
			if (e.type == SDL_QUIT)
				result = true;
			else if (e.type == SDL_MOUSEBUTTONDOWN)
				HandleMouseButtonDown(e);
			else if (e.type == SDL_MOUSEBUTTONUP)
				HandleMouseButtonUp(e);
			else if (e.type == SDL_MOUSEWHEEL)
				HandleMouseWheel(e);
			else if (e.type == SDL_TEXTINPUT)
				HandleText(e);
			else if (e.type == SDL_KEYDOWN)
				HandleKeyboard(e);
		}

		int mousex, mousey;

		SDL_GetMouseState(&mousex, &mousey);

		_mousePosition = glm::vec2(float(mousex), _windowSize.y - float(mousey));

		return result;
	}

	void InputState::HandleMouseButtonDown(SDL_Event pE)
	{
		if (pE.button.button == SDL_BUTTON_LEFT)
			_leftButtonDown = true;
		else if (pE.button.button == SDL_BUTTON_RIGHT)
			_rightButtonDown = true;
	}

	void InputState::HandleMouseButtonUp(SDL_Event pE)
	{
		if (pE.button.button == SDL_BUTTON_LEFT)
			_leftButtonDown = false;
		else if (pE.button.button == SDL_BUTTON_RIGHT)
			_rightButtonDown = false;
	}

	void InputState::HandleMouseWheel(SDL_Event pE)
	{
		_wheelAmount = pE.wheel.y;
	}

	void InputState::HandleText(SDL_Event e)
	{
		_text += e.text.text;
	}

	void InputState::HandleKeyboard(SDL_Event e)
	{
		if (e.key.keysym.sym == SDLK_BACKSPACE)
			_backspacePressed = true;
	}

	std::string InputState::UpdateText(std::string pText)
	{ 
		pText += _text;
		if (_backspacePressed && pText.size() > 0)
			pText.pop_back();

		return pText;
	}
}