//License : Please see Rarium.LICENSE
#ifndef _RariumPluginManager_

#define _RariumPluginManager_

#include <Rarium/Core/Rarium.h>
#include <vector>
#include <map>
#include <Rarium/Core/Event.h>

namespace Rarium
{
	/**
	* Singleton used to manage plugins.
	* Plugins are dll hot loaded at the start of the application.
	*/
	class Rarium_API PluginManager
	{
	public:
		/// Load all plugins found in the plugin directory
		void LoadPlugins();

		/// Get all file handle for each plugins
		inline std::vector<void*> GetPlugins() { return _plugins; }
		/// Get all features avaible, with their corresponding plugins.
		inline std::map<std::string, std::vector<void*>> GetFeatures() { return _features; }
		/// Get all file handle of plugins that provide a given feature
		inline std::vector<void*> GetFeaturesByType(std::string pType) { return _features.find(pType) != _features.cend() ? _features[pType] : std::vector<void*>(); }

		/// Get all the data from plugins for a given feature
		template<typename T>
		std::vector<T> GetFeatureData(std::string pFeatureName)
		{
			std::vector<T> result;

			for (void* handle : _features[pFeatureName])
			{
				std::vector<T>*(*func)();
				func = (std::vector<T>*(*)())SDL_LoadFunction(handle, std::string("Get"+pFeatureName).c_str());
				if (func != nullptr)
				{
					std::vector<T>* featureData = func();

					for (T data : (*featureData))
					{
						result.push_back(data);
					}

					delete featureData;
				}
				else
					Log("Unable to load Get"+ pFeatureName +" function in a plugin");
			}

			return result;
		}

		/// Get a reference to the unique instance of the manager
		static inline PluginManager& GetRef() { return _instance; }

	protected:
		/// Build the plugin manager
		PluginManager();
		PluginManager(PluginManager&) = delete;
		PluginManager(PluginManager&&) = delete;
		PluginManager operator=(PluginManager&) = delete;
		~PluginManager();

		std::vector<void*> _plugins;///< List of all handle to the plugin files
		std::map<std::string, std::vector<void*>> _features;///< List of all features avaible and their associated plugins

		static PluginManager _instance;///< Unique instance of the manager
	};
}

#endif