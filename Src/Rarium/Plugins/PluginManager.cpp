//License : Please see Rarium.LICENSE
#include <Rarium/Plugins/PluginManager.h>
#include <SDL2/SDL.h>
#include <tinydir/tinydir.h>
#include <Rarium/Core/Log.h>

#include <Rarium/Ui/Ui.h>

#ifdef _DEBUG
#define PLUGINPATH "./PluginsDebug/"
#else
#define PLUGINPATH "./Plugins/"
#endif

namespace Rarium
{
	PluginManager::PluginManager()
	{

	}

	PluginManager::~PluginManager()
	{
		for (void* p : _plugins)
		{
			SDL_UnloadObject(p);
		}
	}

	void PluginManager::LoadPlugins()
	{
		tinydir_dir dir;
		if (tinydir_open(&dir, PLUGINPATH) == -1)
		{
			Log("Error opening file");
		}

		int cpt = 0;

		while (dir.has_next)
		{
			tinydir_file file;
			if (tinydir_readfile(&dir, &file) == -1)
			{
				Log("Error getting file");
			}

			if (std::string(file.extension) == "dll")
			{
				void* handle = nullptr;
				handle = SDL_LoadObject(file.path);
				if (handle != nullptr)
				{
					std::vector<std::string>* (*func)();
					func = (std::vector<std::string>*(*)())SDL_LoadFunction(handle, "GetPluginFeatures");
					if (func != nullptr)
					{
						std::vector<std::string>* pluginFeatures = func();
						_plugins.push_back(handle);

						for (std::string& str : (*pluginFeatures))
						{
							_features[str].push_back(handle);
						}

						delete pluginFeatures;
					}
					else
						Log("Unable to load GetPluginFeatures function in : " + std::string(file.path));
				}
				else
					Log("Unable to load plugin : "+std::string(file.path));
			}

			tinydir_next(&dir);
		}
		tinydir_close(&dir);
	}

	PluginManager PluginManager::_instance;
}