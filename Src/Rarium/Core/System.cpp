//License : Please see Rarium.LICENSE
#include <Rarium/Core/System.h>
#include <Rarium/Core/Log.h>
#include <Rarium/Core/Clock.h>
#include <Rarium/Core/Exception.h>
#include <Rarium/Core/Math.h>
#include <Rarium/Plugins/PluginManager.h>
#include <SDL2/SDL_ttf.h>

namespace Rarium
{
	System::System(bool pDisplayMenu)
	{
		_controllerPtr = nullptr;

		_running = true;
		_skipMouse = false;
		_error = false;
		_uiChangeRequested = false;
		_displayMenu = pDisplayMenu;
	}

	System::~System()
	{
		for (auto it = _uis.cbegin(); it != _uis.cend(); it++)
			delete it->second;
	}

	void System::Run()
	{		
		try
		{
			Log("==============================================================");
			Log("=========================Launching  !=========================");
			Log("==============================================================");

			//Initialize everything, at each step we check running to be sure the previous step go init correctly
			if(InitEngine())
			{
				Log("Window opened, Width: "+ToString(_width)+"px, Height: "+ToString(_height)+"px");

				if (!_renderer.Init(_width, _height,this))
				{
					_error = true;
					Log("Renderer Init Error");
				}

				LoadUis();

				_renderer.SetMenu(_menu.get());

				_currentInput.SetWindowSize(vec2(_width, _height));
				_currentUiDescriptor = _uis[_startupUiName];
				_currentUi = _currentUiDescriptor->GetUi();
				_currentUi->SetUiFunction(this);
				_renderer.SetupUi(_currentUi);


				
				//Reset the clock for delta time calculation
				Clock::GetRef().GetElapsedTime();
				try
				{
					while(_running && !_error)
					{
						_running = !_currentInput.Update();

						if (_uiChangeRequested)
							ChangeUi();

						UiElement::ResetMouseInterception();

						float delta = Clock::GetRef().GetElapsedTime();

						if(_currentUiDescriptor->DisplayMenu())
							_menu->Update(delta,&_currentInput, &_lastInput);

						_currentUi->Update(delta,&_currentInput,&_lastInput);

						_renderer.DrawFrame(delta,_displayMenu && _currentUiDescriptor->DisplayMenu());

						SDL_GL_SwapWindow(_mainwindowPtr);

						_lastInput = _currentInput;
					}
				}
				catch(Exception e)
				{
					ReportError(e.what());
				}

				_renderer.Unload();
				StopEngine();
			}
		}
		catch(Exception e)
		{
			ReportError(e.what());
			StopEngine();
		}
	}

	bool System::InitEngine()
	{
		if (SDL_Init(SDL_INIT_VIDEO | SDL_INIT_JOYSTICK | SDL_INIT_GAMECONTROLLER) < 0)
		{
			ReportError("Erreur lors de l'initialisation de la SDL : " + std::string(SDL_GetError()));
			SDL_Quit();
			return false;
		}
		else
			Log("SDL Initialized !");

		if (TTF_Init() < 0)
		{
			TTF_Quit();
			SDL_Quit();
			return false;
		}
			else
				Log("SDL_TTF Initialized !");

		//Set sdl gl attribute
		SDL_GL_SetAttribute(SDL_GL_CONTEXT_MAJOR_VERSION, 4);
		SDL_GL_SetAttribute(SDL_GL_CONTEXT_MINOR_VERSION, 5);
		SDL_GL_SetAttribute(SDL_GL_CONTEXT_PROFILE_MASK, SDL_GL_CONTEXT_PROFILE_CORE );
		SDL_GL_SetAttribute(SDL_GL_MULTISAMPLEBUFFERS,1);
		SDL_GL_SetAttribute(SDL_GL_MULTISAMPLESAMPLES,4);
		SDL_GL_SetAttribute(SDL_GL_RED_SIZE,8);
		SDL_GL_SetAttribute(SDL_GL_GREEN_SIZE,8);
		SDL_GL_SetAttribute(SDL_GL_BLUE_SIZE,8);
		SDL_GL_SetAttribute(SDL_GL_DEPTH_SIZE,24);
#ifdef _DEBUG
		SDL_GL_SetAttribute(SDL_GL_CONTEXT_FLAGS, SDL_GL_CONTEXT_DEBUG_FLAG);
#endif

		_mainwindowPtr = SDL_CreateWindow("Rarium", SDL_WINDOWPOS_CENTERED, SDL_WINDOWPOS_CENTERED,
			1280, 720, SDL_WINDOW_OPENGL | SDL_WINDOW_SHOWN);
		_width = 1280;
		_height = 720;

		if (!_mainwindowPtr) /* Die if creation failed */
		{
			Log("SDL Window creation failed : " + std::string(SDL_GetError()));
			TTF_Quit();
			SDL_Quit();
			return false;
		}
		else
			Log("Window created");

		_maincontext = SDL_GL_CreateContext(_mainwindowPtr);

		SDL_GL_SetSwapInterval(1);

		//SDL_ShowCursor(SDL_DISABLE);

		SDL_FlushEvents(SDL_FIRSTEVENT,SDL_LASTEVENT);

		PluginManager::GetRef().LoadPlugins();

		SDL_StartTextInput();

		static SDL_Rect rect;
		rect.x = 0;
		rect.y = 0;
		rect.w = 1280;
		rect.h = 720;

		SDL_SetTextInputRect(&rect);


		return true;
	}

	void* System::GetGLProcAddressPtr(std::string procName)
	{
		return SDL_GL_GetProcAddress(procName.c_str());
	}

	void System::StopEngine()
	{
		TTF_Quit();
		SDL_Quit();
	}

	void System::ReportError(std::string pError)
	{
		Log("MASSIVE ERROR : " + pError);
		SDL_ShowSimpleMessageBox(SDL_MESSAGEBOX_ERROR, "Error", pError.c_str(), nullptr);
		_error = true;
	}

	void System::ChangeUi()
	{
		_uiChangeRequested = false;

		delete _currentUi;
		if (_uis.find(_uiWanted) == _uis.cend())
		{
			throw Exception("Trying to request an ui that doesn't exist.");
		}
		else
		{
			_currentUiDescriptor = _uis[_uiWanted];
			_currentUi = _currentUiDescriptor->GetUi();
			_currentUi->SetUiFunction(this);
			_renderer.SetupUi(_currentUi);
			Log("Ui Changed : " + _uiWanted);
		}
	}

	void System::RequestUiChange(std::string pUi)
	{
		if (_currentUiDescriptor->GetName() != pUi)
		{
			_uiChangeRequested = true;
			_uiWanted = pUi;
		}
	}

	void System::LoadUis()
	{
		std::vector<UiDescriptor*> startupUi = PluginManager::GetRef().GetFeatureData<UiDescriptor*>("StartupUi");

		if (startupUi.size() == 0)
		{
			throw Exception("No Startup Ui Found in plugins");
		}
		else
		{
			_startupUiName = startupUi.front()->GetName();
			_uis.insert(std::make_pair(startupUi.front()->GetName(), startupUi.front()));
		}

		std::vector<UiDescriptor*> uis = PluginManager::GetRef().GetFeatureData<UiDescriptor*>("Uis");

		for (UiDescriptor* desc : uis)
		{
			_uis.insert(std::make_pair(desc->GetName(), desc));
		}

		uis.push_back(startupUi.front());

		_fakeMenuUi.SetUiFunction(this);
		_menu = std::make_unique<MenuElement>(&_fakeMenuUi,"RariumBasicMenuElement", uis);
	}

	bool System::_error;
}