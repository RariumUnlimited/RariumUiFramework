//License : Please see Rarium.LICENSE
#ifndef _RariumLog_

#define _RariumLog_

#include <Rarium/Core/Rarium.h>
#include <Rarium/Core/Util.h>
#include <fstream>

#include <SDL2/SDL.h>

namespace Rarium
{
	/**
	*	Singleton used to log message to a file
	*/
	class Logger
	{
	public:
		/**
		*	Log a message
		*	@param pMessage Message to log
		*/
		void LogMessage(std::string pMessage);
		/**
		*	Get the Logger singleton
		*	@return The reference to the unique instance of the Logger
		*/
		static Logger& GetRef() { return _instance; }

	protected:
		std::ofstream _ofs;///< Stream where we write messages


	private:
		/// Build a logger
		Logger();
		Logger(Logger&) = delete;
		Logger(Logger&&) = delete;
		Logger operator=(Logger&) = delete;
		static Logger _instance;///Unique instance of the logger
	};
	#define Log(message) Rarium::Logger::GetRef().LogMessage(Rarium::ToString(message));
}



#endif