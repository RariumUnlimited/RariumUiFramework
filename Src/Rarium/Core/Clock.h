//License : Please see Rarium.LICENSE
#ifndef _RariumClock_

#define _RariumClock_

#include <Rarium/Core/Rarium.h>

#include <ctime>

namespace Rarium
{
	/**
	*	Time mesurement while the program is running
	*/
	class Rarium_API Clock
	{
	public:
		/**
		*	Return for how long the program has been running
		*	@return The total time the program has been running in second
		*/
		float GetTotalTime();
		/**
		*	Get how many seconds passed since the last call of GetElapsedTime
		*	@return The elapsed time in seconds
		*/
		float GetElapsedTime();
		/**
		*	Begin recording time, call EndRecording after to know how many seconds passed between the two call.
		*/
		void BeginRecording();
		/**
		*	Get the recording resut.
		*	@return How many seconds between BeginRecording and this call
		*/
		float EndRecording();

		/**
		*	Get our Clock.
		*	@return Reference to our clock instance
		*/
		static inline Clock& GetRef() { return instance; }

	protected:

		unsigned int _lastCheck;///< Time at the last call of GetElapsedTime
		unsigned int _recordBegin;///< Time at which we called BeginRecording

	private:
		/// Build a new clock
		Clock();
		Clock(Clock&) = delete;
		Clock(Clock&&) = delete;
		Clock& operator=(Clock&) = delete;
		static Clock instance;///< Instance of our clock
	};
}

#endif