//License : Please see Rarium.LICENSE
#include <Rarium/Core/File.h>
#include <SDL2/SDL.h>
#include <Rarium/Core/Exception.h>

namespace Rarium
{
	std::string File::LoadAsString(std::string pPath)
	{
		SDL_RWops* rwPtr = SDL_RWFromFile((GetBasePath() + pPath).c_str(), "rb");
		Sint64 length = 0;

		if (rwPtr != nullptr)
		{
			length = SDL_RWseek(rwPtr, 0, SEEK_END);

			if (length < 0)
			{
				throw Exception("Could not seek inside : " + pPath);
			}
		}
		else
		{
			throw Exception("Could not read file : " + pPath);
		}

		SDL_RWseek(rwPtr, 0, RW_SEEK_SET);

		char* dataAr = new char[length + 1];
		//We put an \0 in the case the content of the file is read with a string stream
		dataAr[length] = '\0';

		SDL_RWread(rwPtr, dataAr, length, 1);

		SDL_RWclose(rwPtr);

		std::string result(dataAr, length);

		delete dataAr;

		return result;
	}

	std::vector<char> File::LoadAsVector(std::string pPath)
	{
		SDL_RWops *rwPtr = SDL_RWFromFile((GetBasePath() + pPath).c_str(), "rb");
		Sint64 length = 0;

		if (rwPtr != nullptr)
		{
			length = SDL_RWseek(rwPtr, 0, SEEK_END);

			if (length < 0)
			{
				throw Exception("Could not seek inside : " + pPath);
			}
		}
		else
		{
			throw Exception("Could not read file : " + pPath);
		}

		SDL_RWseek(rwPtr, 0, RW_SEEK_SET);

		std::vector<char> result;

		result.resize(length);

		SDL_RWread(rwPtr, &result[0], length, 1);

		SDL_RWclose(rwPtr);

		return result;
	}


	std::string File::GetBasePath()
	{
		return "./Assets/";
	}
}