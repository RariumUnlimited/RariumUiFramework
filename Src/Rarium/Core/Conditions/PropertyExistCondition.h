//License : Please see Rarium.LICENSE
#ifndef _RariumPropertyExistCondition_

#define _RariumPropertyExistCondition_

#include <Rarium/Core/Rarium.h>
#include <Rarium/Core/Condition.h>

#include <string>

namespace Rarium
{
	/**
	*	Condition that check if a user property exist
	*/
	class Rarium_API PropertyExistCondition : public Condition
	{
	public:
		/**
		*	Build the property
		*	@param pCategory Category of the property
		*	@param pName Name of the property
		*/
		PropertyExistCondition(std::string pCategory, std::string pName);
		///Check if the property is true
		virtual bool Check() override;

	protected:
		std::string _category;///< Category of the property
		std::string _name;///< Name of the property
	};
}

#endif