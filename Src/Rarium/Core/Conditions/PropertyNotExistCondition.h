//License : Please see Rarium.LICENSE
#ifndef _RariumPropertyNotExistCondition_

#define _RariumPropertyNotExistCondition_

#include <Rarium/Core/Rarium.h>
#include <Rarium/Core/Condition.h>

#include <string>

namespace Rarium
{
	/**
	*	Condition that check if a user property doesn't exist
	*/
	class Rarium_API PropertyNotExistCondition : public Condition
	{
	public:
		/**
		*	Build the property
		*	@param pCategory Category of the property
		*	@param pName Name of the property
		*/
		PropertyNotExistCondition(std::string pCategory, std::string pName);
		///Check if the property is true
		virtual bool Check() override;

	protected:
		std::string _category;///< Category of the property
		std::string _name;///< Name of the property
	};
}

#endif