//License : Please see Rarium.LICENSE
#include <Rarium/Core/Conditions/PropertyExistCondition.h>
#include <Rarium/UserData/PropertyManager.h>

namespace Rarium
{
	PropertyExistCondition::PropertyExistCondition(std::string pCategory, std::string pName)
	{
		_category = pCategory;
		_name = pName;
	}

	bool PropertyExistCondition::Check()
	{
		return PropertyManager::GetRef().PropertyExists(_category, _name);
	}
}