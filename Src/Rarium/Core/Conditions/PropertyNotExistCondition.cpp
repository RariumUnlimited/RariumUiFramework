//License : Please see Rarium.LICENSE
#include <Rarium/Core/Conditions/PropertyNotExistCondition.h>
#include <Rarium/UserData/PropertyManager.h>

namespace Rarium
{
	PropertyNotExistCondition::PropertyNotExistCondition(std::string pCategory, std::string pName)
	{
		_category = pCategory;
		_name = pName;
	}

	bool PropertyNotExistCondition::Check()
	{
		return !PropertyManager::GetRef().PropertyExists(_category, _name);
	}
}