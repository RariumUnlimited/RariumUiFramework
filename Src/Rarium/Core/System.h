//License : Please see Rarium.LICENSE
#ifndef _RariumSystem_

#define _RariumSystem_

#include <string>
#include <memory>

#include <SDL2/SDL.h>
#include <Rarium/Core/Rarium.h>
#include <Rarium/Render/Renderer.h>
#include <Rarium/Input/InputState.h>
#include <Rarium/Ui/UIFunction.h>
#include <Rarium/Ui/UiDescriptor.h>
#include <Rarium/Ui/UiElements/MenuElement.h>

namespace Rarium
{
	class Rarium_API System : public UiFunction
	{
	public:
		/**
		*	Build the engine
		*	@param appLogic Update logic for the user app
		*/
		System(bool pDisplayMenu = true);
		/**
		*	Destroy our engine
		*/
		~System();

		/**
		*	Launch the app
		*/
		void Run();

		/**
		*	Get the state of user input state of the current frame
		*	@return Pointer to the input state of the current state
		*/
		inline InputState* GetCurrentInputPtr() { return &_currentInput; }
		/**
		*	Get the state of user input state of the last frame
		*	@return Pointer to the input state of the last state
		*/
		inline InputState* GetLastInputPtr() { return &_lastInput; }

		/**
		*	Return the adresse of an opengl function
		*	@param procName The name of the function
		*	@return Adress of the function
		*/
		static void* GetGLProcAddressPtr(std::string pProcName);
		/**
		*	Report an error to the engine
		*	@param error The message of the error
		*/
		static void ReportError(std::string pError);

		//===============<UiFunction>====================
		virtual void RequestUiChange(std::string pUi) override;
		//===============<UiFunction>====================

	protected:
		/**
		*	Open the window and initialize sdl/opengl
		*	@return True if the window opened successfully
		*/
		bool InitEngine();
		/**
		*	Shut down sdl
		*/
		void StopEngine();
		/**
		*	Change the current ui displayed
		*/
		void ChangeUi();
		/**
		*	Load all the user interfaces in memory
		*/
		void LoadUis();

		Renderer _renderer;///< OpenGL renderer used to draw uis

		SDL_Window* _mainwindowPtr; ///< Our sdl window
		SDL_GLContext _maincontext;///< Our opengl context

		Ui* _currentUi;///< Pointer to the currently displayed ui
		UiDescriptor* _currentUiDescriptor;///< Pointer to the current ui descriptor
		std::unique_ptr<MenuElement> _menu;///< Pointer to the Menu Element
		Ui _fakeMenuUi;///< A fake ui that is used to display the Menu Element

		bool _displayMenu;///< Yes or no if the menu can be displayed

		InputState _currentInput;///< User input state of the current frame
		InputState _lastInput;///< User input state of the last frame

		static bool _error;///< If true then an error occured and the app must quit

		unsigned int _width;///< Width of our window
		unsigned int _height;///< Height of our window

		bool _running;///< To know if the app is running
		bool _skipMouse;///< To know if we must skip mouse update next frame

		SDL_GameController* _controllerPtr;///< Main game controller of the app

		bool _uiChangeRequested;///< If a ui change has been requested
		std::string _uiWanted;///< Name of the ui to change to

		std::string _startupUiName;///< Name of the ui to display at the application startup

		std::map<std::string, UiDescriptor*> _uis;///< List of all uis, Name -> Descriptor
	};
}

#endif