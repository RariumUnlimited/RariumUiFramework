//License : Please see Rarium.LICENSE
#ifndef _RariumCondition_

#define _RariumCondition_

#include <Rarium/Core/Rarium.h>

namespace Rarium
{
	/**
	Condition checking object
	*/
	class Rarium_API Condition
	{
	public:
		///Check if the condition is true
		virtual bool Check() { return true; }
	protected:

	};
}

#endif