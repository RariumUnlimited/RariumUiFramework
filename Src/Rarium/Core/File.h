//License : Please see Rarium.LICENSE
#ifndef _RariumFile_

#define _RariumFile_

#include <Rarium/Core/Rarium.h>

#include <string>
#include <vector>

namespace Rarium
{
	class Rarium_API File
	{
	public:
		/**
		*	Load a file as a string
		*	@param pPath Path to the file in the assets directory
		*	@return A string which represent the content of a file
		*/
		static std::string LoadAsString(std::string pPath);
		/**
		*	Load a file as a vector of char
		*	@param pPath Path to the file in the assets directory
		*	@return A vector which contains the files
		*/
		static std::vector<char> LoadAsVector(std::string pPath);
		/**
		*	Get the path of the assets directory
		*	@return Path to the assets relative to the current location of the executable file
		*/
		static std::string GetBasePath();
	};
}

#endif