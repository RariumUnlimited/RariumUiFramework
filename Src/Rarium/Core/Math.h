//License : Please see Rarium.LICENSE
#ifndef _RariumMath_

#define _RariumMath_

#include <Rarium/Core/Rarium.h>

namespace Rarium
{
	/**
	*	Template function to clamp a value between a min and a max
	*	@param value Value to clamp
	*	@param min Minimum value
	*	@param max Maximum value
	*	@return min if value < min, max if value > max or value
	*/
	template <typename T> inline T Rarium_API Clamp(T pValue, T pMin, T pMax)
	{
		return pValue <= pMax ? (pValue >= pMin ? pValue : pMin) : pMax;
	}
}

#endif