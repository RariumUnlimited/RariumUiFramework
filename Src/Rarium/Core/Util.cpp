//License : Please see Rarium.LICENSE
#include <Rarium/Core/Util.h>

namespace Rarium
{
	std::vector<std::string> Split(std::string pStringToSplit, char pSeparator)
	{
		//Where we store the list of strings
		std::vector<std::string> strings;

		//Store the value of the current split string
		std::string currentString;

		for (unsigned int i = 0; i < pStringToSplit.size(); i++)
		{
			//If we encounter a space while we are not in an argument it means it's the end of the current argument
			if (pStringToSplit[i] == pSeparator)
			{
				if (currentString != "")
				{
					strings.push_back(currentString);
					currentString = "";
				}
			}
			else
			{
				currentString += pStringToSplit[i];
			}
		}

		//If we have something in the value of the current, we must add it to the argument list.
		if (currentString != "")
		{
			strings.push_back(currentString);
			currentString = "";
		}

		return strings;
	}
}