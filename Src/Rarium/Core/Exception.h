//License : Please see Rarium.LICENSE
#ifndef _RariumException_

#define _RariumException_

#include <Rarium/Core/Rarium.h>

#include <string>
#include <stdexcept>

namespace Rarium
{
	/**
	*	Exception that can be throw by engine components
	*/
	class Rarium_API Exception : public std::exception
	{
	public:
		/**
		*	Create a new exception
		*	@param message Message describing what is the exception
		*/
		Exception(std::string pMessage) { _message = pMessage; }
		/**
		*	Get what the exception is about
		*	@return The exception message
		*/
		virtual const char* what() const override {return _message.c_str();}
	protected:
		std::string _message;///< Message describing the exception
	};
}

#endif