//License : Please see Rarium.LICENSE
#ifndef _RariumUtil_

#define _RariumUtil_

#include <vector>
#include <string>
#include <Rarium/Core/Rarium.h>
#include <sstream>

namespace Rarium
{
	/**
	*	Convert an object to a string.
	*/
	template <typename T> inline std::string ToString(const T& pT)
	{
		std::ostringstream os;
		os << pT;
		return os.str();
	}

	/**
	*	Convert a string to an object.
	*/
	template <typename T> inline T ToObject(const std::string& pStr)
	{
		T result;
		std::istringstream is(pStr);
		is >> result;
		return result;
	}

	/**
	*	Split a string in a vector of string depending on a specific character
	*	@param stringToSplit The string that we want to split
	*	@param seperator The char used to know when we need to split
	*	@return A vector containing all the substring of stringToSplit
	*/
	std::vector<std::string> Rarium_API Split(std::string pStringToSplit, char pSeparator);
}

#endif