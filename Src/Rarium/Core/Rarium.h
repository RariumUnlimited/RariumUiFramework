//License : Please see Rarium.LICENSE
#ifndef _RariumRarium_

#define _RariumRarium_

#ifdef RARIUM_EXPORTS
	#ifdef WIN32
		#define Rarium_API __declspec(dllexport) 
	#else
		#define Rarium_API
	#endif
#else
	#ifdef WIN32
		#define Rarium_API __declspec(dllimport) 
	#else
		#define Rarium_API
	#endif
#endif

#endif