//License : Please see Rarium.LICENSE
#include <Rarium/Core/Log.h>
#include <chrono>
#include <ctime>

namespace Rarium
{
	Logger Logger::_instance;

	void Logger::LogMessage(std::string pMessage)
	{
		std::chrono::system_clock::time_point today = std::chrono::system_clock::now();
		std::time_t tt = std::chrono::system_clock::to_time_t(today);

		std::string timeStr = ctime(&tt);
		timeStr.pop_back();
		pMessage = timeStr + " : " + pMessage;

		SDL_LogMessage(SDL_LOG_CATEGORY_APPLICATION, SDL_LOG_PRIORITY_INFO, pMessage.c_str());

		_ofs << pMessage << std::endl;
	}

	Logger::Logger()
	{
		_ofs.open("Log.txt", std::ofstream::out | std::ofstream::app);
	}
}