//License : Please see Rarium.LICENSE
#include <Rarium/Core/Clock.h>
#include <SDL2/SDL.h>

namespace Rarium
{
	Clock::Clock()
	{
		_lastCheck = clock();
	}
	
	float Clock::GetTotalTime()
	{
		return SDL_GetTicks()/1000.f;
	}

	float Clock::GetElapsedTime()
	{
		unsigned int temp = SDL_GetTicks();
		float result = (temp - _lastCheck)/1000.f;
		_lastCheck = temp;
		return result;
	}
	
	void Clock::BeginRecording()
	{
		_recordBegin = SDL_GetTicks();
	}

	float Clock::EndRecording()
	{
		return (SDL_GetTicks() - _recordBegin) / 1000.f;
	}

	Clock Clock::instance;
}