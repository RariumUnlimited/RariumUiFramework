//License : Please see Rarium.LICENSE
#ifndef _RariumGLUtil_

#define _RariumGLUtil_

#include <Rarium/Core/Rarium.h>
#include <string>
#include <glbinding/gl/gl.h>
#include <glbinding/Binding.h>

using namespace gl;

namespace Rarium
{
	class Rarium_API GLUtil
	{
	public:
		/**
		* Check if there is an opengl error, throw an exception if it is the case.
		*/
		static void TreatGLError(std::string pFile, std::string pLine);
		/**
		*	Get the string corresponding to an opengl debug source
		*	@param source OpenGL debug source
		*	@return String containing the name of this debug source
		*/
		static std::string SourceToStr(GLenum pSource);
		/**
		*	Get the string corresponding to an opengl debug type
		*	@param type OpenGL debug type
		*	@return String containing the name of this debug type
		*/
		static std::string TypeToStr(GLenum pType);
		/**
		*	Get the string corresponding to an opengl debug severity
		*	@param severity OpenGL debug severity
		*	@return String containing the name of this debug severity
		*/
		static std::string SeverityToStr(GLenum pSeverity);
		/**
		*	Get the string corresponding to a glsl type
		*	@param type GLSL type
		*	@return String containing the name of this type
		*/
		static std::string GLSLTypeToStr(GLenum pType);
		/**
		*	Get the size of a glsl type on byte
		*	@param GLSL type
		*	@return The size in byte of the type
		*/
		static GLsizei SizeofGLSLType(GLenum pType);
		/**
		*	Get the base type of a glsltype
		*	@param type GLSL type
		*	@return The base type of type
		*/
		static GLenum GLSLTypeToBaseType(GLenum pType);
		/**
		*	Get the number of component of a type
		*	@param type GLSL Type
		*	@return Number of component of type
		*/
		static GLuint GLSLTypeToComponentCount(GLenum pType);
	};
}

#endif