//License : Please see Rarium.LICENSE
#include <Rarium/Render/Renderer.h>
#include <Rarium/Core/File.h>
#include <Rarium/Core/Exception.h>
#include <Rarium/Ui/UiElements/TextElement.h>
#include <Rarium/Ui/UiElements/ButtonElement.h>
#include <Rarium/Ui/UiElements/ShaderToyElement.h>
#include <Rarium/Ui/UiElements/MultiTextElement.h>
#include <Rarium/Core/Clock.h>
#include <glbinding/ContextInfo.h>
#include <glbinding/Meta.h>

namespace Rarium
{
	bool Renderer::Init(unsigned int pWidth, unsigned int pHeight, UiFunction* pUiFunctionPtr)
	{
		_uiFunctionPtr = pUiFunctionPtr;
		_width = pWidth;
		_height = pHeight;

		glbinding::Binding::initialize();

		//Get context info
		Log("GL Info : ");
		Log(ToString(glGetString(GL_VENDOR)));
		Log(ToString(glGetString(GL_RENDERER)));
		Log(ToString(glGetString(GL_VERSION)));
		Log(ToString(glGetString(GL_SHADING_LANGUAGE_VERSION)));

		debugProc = GLDEBUGPROC(&DebugProc);
		glDebugMessageCallback(debugProc, nullptr);

		glCullFace(GL_BACK);
		glFrontFace(GL_CCW);
		glEnable(GL_DEPTH_TEST);
		glDepthFunc(GL_LEQUAL);
		glClearColor(0, 0, 0, 1);
		glClearDepthf(1.0);
		glEnable(GL_BLEND);
		glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);

		glEnable(GL_TEXTURE_CUBE_MAP_SEAMLESS);

		glGenVertexArrays(1, &_baseVAO);

		SetupPrograms();
		SetupBuffers();

		///TODO : Check for ATI memory info
		std::set<GLextension> extensions = ContextInfo::extensions();

		if (extensions.find(GLextension::GL_NVX_gpu_memory_info) != extensions.cend())
		{
			GLint memory = 0;
			glGetIntegerv(GL_GPU_MEMORY_INFO_DEDICATED_VIDMEM_NVX, &memory);
			Log("Dedicated video memory, total size(in kb) of the GPU memory : " + ToString(memory));
			glGetIntegerv(GL_GPU_MEMORY_INFO_TOTAL_AVAILABLE_MEMORY_NVX, &memory);
			Log("Total available memory, total size(in Kb) of the memory available for allocations : " + ToString(memory));
			glGetIntegerv(GL_GPU_MEMORY_INFO_CURRENT_AVAILABLE_VIDMEM_NVX, &memory);
			Log("Current available dedicated video memory(in kb), currently unused GPU memory : " + ToString(memory));
			glGetIntegerv(GL_GPU_MEMORY_INFO_EVICTION_COUNT_NVX, &memory);
			Log("Count of total evictions seen by system : " + ToString(memory));
			glGetIntegerv(GL_GPU_MEMORY_INFO_EVICTED_MEMORY_NVX, &memory);
			Log("Size of total video memory evicted(in kb) : " + ToString(memory));
		}


		CheckGLError();

		_uiPtr = nullptr;
		_menuPtr = nullptr;

		return true;
	}

	void Renderer::DrawFrame(float pDelta, bool pDisplayMenu)
	{
		UpdateDynamicElements();

		glMemoryBarrier(GL_CLIENT_MAPPED_BUFFER_BARRIER_BIT);

		glViewport(0, 0, _width, _height);

		glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

		DrawUi(pDelta);

		if (pDisplayMenu && _menuPtr != nullptr)
			DrawMenu(pDelta);

		CheckGLError();
	}

	void Renderer::DrawUi(float pDelta)
	{
		if (_uiPtr != nullptr)
		{
			for (UiElement* element : _uiPtr->GetElements())
			{
				element->Draw();
			}
		}
	}
	
	void Renderer::DrawMenu(float pDelta)
	{
		_menuPtr->Draw();
	}

	void Renderer::SetupUi(Ui* pUiPtr)
	{
		UnloadAssets();

		_uiPtr = pUiPtr;
		for (UiElement* elem : _uiPtr->GetElements())
		{
			ProcessElement(elem);
		}

		if(_menuPtr != nullptr)
			ProcessElement(_menuPtr);

		CheckGLError();
	}

	void Renderer::SetMenu(UiElement* pMenuPtr)
	{
		_menuPtr = pMenuPtr;
	}

	void GL_APIENTRY Renderer::DebugProc(GLenum pSource, GLenum pType, GLuint pId, GLenum pSeverity, GLsizei pLength, const GLchar* pMessageAR, void* pUserParamPtr)
	{
		Log("OpenGL message, Source " + GLUtil::SourceToStr(pSource) + ", Type " + GLUtil::TypeToStr(pType) + ", Severity " + GLUtil::SeverityToStr(pSeverity) + " : " + ToString(pMessageAR));
	}

	void Renderer::DrawImage(std::string pPath, vec2 pPosition)
	{
		Program* program = _programs["Texture"];
		glUseProgram(program->GetProgram());
		glBindVertexArray(_baseVAO);
		glUniform4f(program->GetUniforms()["PositionSize"].Location, pPosition.x, pPosition.y, (GLfloat)_userTextures[pPath]->GetWidth(), (GLfloat)_userTextures[pPath]->GetHeight());
		glUniformHandleui64ARB(program->GetUniforms()["Texture"].Location, _userTextures[pPath]->GetHandle());
		glDrawArrays(GL_TRIANGLE_STRIP, 0, 4);
	}

	void Renderer::DrawImage(std::string pPath, vec2 pPosition, vec2 pSize)
	{
		Program* program = _programs["Texture"];
		glUseProgram(program->GetProgram());
		glBindVertexArray(_baseVAO);
		glUniform4f(program->GetUniforms()["PositionSize"].Location, pPosition.x, pPosition.y, pSize.x, pSize.y);
		glUniformHandleui64ARB(program->GetUniforms()["Texture"].Location, _userTextures[pPath]->GetHandle());
		glDrawArrays(GL_TRIANGLE_STRIP, 0, 4);
	}

	void Renderer::DrawImage(GLuint64 pHandle, vec2 pPosition, vec2 pSize)
	{
		Program* program = _programs["Texture"];
		glUseProgram(program->GetProgram());
		glBindVertexArray(_baseVAO);
		glUniform4f(program->GetUniforms()["PositionSize"].Location, pPosition.x, pPosition.y, pSize.x, pSize.y);
		glUniformHandleui64ARB(program->GetUniforms()["Texture"].Location, pHandle);
		glDrawArrays(GL_TRIANGLE_STRIP, 0, 4);
	}

	void Renderer::DrawText(std::string pPath, vec2 pPosition)
	{
		Program* program = _programs["Texture"];
		glUseProgram(program->GetProgram());
		glBindVertexArray(_baseVAO);
		glUniform4f(program->GetUniforms()["PositionSize"].Location, pPosition.x, pPosition.y, (GLfloat)_userTexts[pPath]->GetWidth(), (GLfloat)_userTexts[pPath]->GetHeight());
		glUniformHandleui64ARB(program->GetUniforms()["Texture"].Location, _userTexts[pPath]->GetHandle());
		glDrawArrays(GL_TRIANGLE_STRIP, 0, 4);
	}

	void Renderer::ShaderToy(std::string pShader, vec2 pPosition, vec2 pSize, std::string pTexture1, std::string pTexture2, std::string pTexture3, std::string pTexture4)
	{
		if (_userPrograms.find(pShader) != _userPrograms.cend())
		{
			Program* program = _userPrograms[pShader];
			glUseProgram(program->GetProgram());
			glBindVertexArray(_baseVAO);
			glUniform4f(program->GetUniforms()["PositionSize"].Location, pPosition.x, pPosition.y, pSize.x, pSize.y);
				
			if(pTexture1 != "" && program->IsUniformActive("Texture0"))
				glUniformHandleui64ARB(program->GetUniforms()["Texture0"].Location, _userTextures[pTexture1]->GetHandle());
			if (pTexture2 != "" && program->IsUniformActive("Texture1"))
				glUniformHandleui64ARB(program->GetUniforms()["Texture1"].Location, _userTextures[pTexture2]->GetHandle());
			if (pTexture3 != "" && program->IsUniformActive("Texture2"))
				glUniformHandleui64ARB(program->GetUniforms()["Texture2"].Location, _userTextures[pTexture3]->GetHandle());
			if (pTexture4 != "" && program->IsUniformActive("Texture3"))
				glUniformHandleui64ARB(program->GetUniforms()["Texture3"].Location, _userTextures[pTexture4]->GetHandle());

			if (program->IsUniformActive("iGlobalTime"))
				glUniform1f(program->GetUniforms()["iGlobalTime"].Location, Clock::GetRef().GetTotalTime());
				
			if (program->IsUniformActive("iMouse"))
			{
				vec4 mouse = vec4(
					_uiFunctionPtr->GetCurrentInputPtr()->IsLeftButtonDown() || _uiFunctionPtr->GetCurrentInputPtr()->IsRightButtonDown() ?
					_uiFunctionPtr->GetCurrentInputPtr()->GetMousePosition() : vec2(0.0,0.0), 
					_uiFunctionPtr->GetCurrentInputPtr()->IsLeftButtonDown() ? 1.0 : 0.0,
					_uiFunctionPtr->GetCurrentInputPtr()->IsRightButtonDown() ? 1.0 : 0.0);

				glUniform4f(program->GetUniforms()["iMouse"].Location,mouse.x,mouse.y,mouse.z,mouse.w);
			}

			glDrawArrays(GL_TRIANGLE_STRIP, 0, 4);
		}
	}

	vec2 Renderer::GetResourceSize(std::string pPath, UiResourceType pRsType)
	{
		vec2 size;
		switch (pRsType)
		{
		case UiResourceType::Texture:
			size = vec2(_userTextures[pPath]->GetWidth(), _userTextures[pPath]->GetHeight());
			break;
		case UiResourceType::Text:
			size = vec2(_userTexts[pPath]->GetWidth(), _userTexts[pPath]->GetHeight());
			break;
		default:
			break;
		}

		return size;
	}

	vec2 Renderer::GetViewportSize()
	{
		return vec2(_width, _height);
	}

	void Renderer::Unload()
	{
		UnloadAssets();

		for (auto it = _programs.cbegin(); it != _programs.cend(); it++)
		{
			delete it->second;
		}

		for (auto it = _shaders.cbegin(); it != _shaders.cend(); it++)
		{
			delete it->second;
		}

		_programs.clear();
		_shaders.clear();

		delete _globalDataBufferPtr; _globalDataBufferPtr = nullptr;
	}

	void Renderer::UnloadAssets()
	{
		for (auto it = _userTextures.cbegin(); it != _userTextures.cend(); it++)
			delete it->second;

		for (auto it = _userTexts.cbegin(); it != _userTexts.cend(); it++)
			delete it->second;

		for (auto it = _userPrograms.cbegin(); it != _userPrograms.cend(); it++)
			delete it->second;

		for (auto it = _userShaders.cbegin(); it != _userShaders.cend(); it++)
			delete it->second;

		_dynamicElements.clear();

		_userTextures.clear();
		_userTexts.clear();
		_userShaders.clear();
		_userPrograms.clear();
	}

	void Renderer::SetupPrograms()
	{
		std::string srcVS = File::LoadAsString("Rarium/Texture.vs.glsl");
		Shader* VSPtr = new Shader("Texture.vs", srcVS);
		std::string srcFS = File::LoadAsString("Rarium/Texture.fs.glsl");
		Shader* FSPtr = new Shader("Texture.fs", srcFS);
		Program* TestProg = new Program(VSPtr, FSPtr);
		TestProg->ExtractData();
		_shaders.insert(std::make_pair("Texture.vs", VSPtr));
		_shaders.insert(std::make_pair("Texture.fs", FSPtr));
		_programs.insert(std::make_pair("Texture", TestProg));
	}

	void Renderer::SetupBuffers()
	{
		_globalDataBufferPtr = new Buffer(sizeof(GlobalData), "Global Data Buffer");
		_globalData.ScreenResolution = vec2(_width, _height);
		_globalDataBufferPtr->Update(&_globalData, sizeof(GlobalData));
		glBindBufferBase(GL_UNIFORM_BUFFER, BlockBinding::GlobalData, _globalDataBufferPtr->GetName());
	}

	void Renderer::UpdateDynamicElements()
	{
		for (UiElement* dynElemPtr : _dynamicElements)
		{
			if (dynElemPtr->RequireUpdate())
			{
				DeleteUiElementResources(dynElemPtr);
				UpdateElement(dynElemPtr);
			}
		}
	}

	void Renderer::ProcessElement(UiElement* pElemPtr)
	{
		for (UiElement* elPtr : pElemPtr->GetChildren())
			ProcessElement(elPtr);

		for (UiResource rs : pElemPtr->GetResources())
		{
			switch (rs.Type)
			{
			case UiResourceType::Texture:
				if (_userTextures.find(rs.Id) == _userTextures.cend())
					_userTextures[rs.Id] = new Texture(dynamic_cast<TextureData*>(rs.RsData)->Path);
				break;
			case UiResourceType::Text:
				LoadTextTexture(rs);
				break;
			case UiResourceType::UserShader:
				LoadShaderToyProgram(rs);
				break;
			default:
				break;
			}
		}

		if (pElemPtr->IsDynamic())
			_dynamicElements.push_back(pElemPtr);

		pElemPtr->UiElementLoaded(this);
	}


	void Renderer::UpdateElement(UiElement* pElemPtr)
	{
		for (UiResource rs : pElemPtr->GetResources())
		{
			if (rs.IsDynamic)
			{
				switch (rs.Type)
				{
				case UiResourceType::Texture:
					_userTextures[rs.Id] = new Texture(dynamic_cast<TextureData*>(rs.RsData)->Path);
					break;
				case UiResourceType::Text:
					LoadTextTexture(rs, true);
					break;
				default:
					break;
				}
			}
		}

		pElemPtr->UiElementLoaded(this);
	}

	void Renderer::LoadTextTexture(UiResource pRs, bool pOverrideExitsCheck)
	{
		TextData* textDataPtr = dynamic_cast<TextData*>(pRs.RsData);
		auto it = _userTexts.find(pRs.Id);
		if (it == _userTexts.cend() || pOverrideExitsCheck)
		{
			if (it != _userTexts.cend() && it->second != nullptr)
				delete _userTexts[pRs.Id];
			TextData* textDataPtr = dynamic_cast<TextData*>(pRs.RsData);
			_userTexts[pRs.Id] = new Texture(textDataPtr->Text, textDataPtr->Font, textDataPtr->FontSize, textDataPtr->Color);
		}
	}

	void Renderer::LoadShaderToyProgram(UiResource pRs, bool pOverrideExitsCheck)
	{
		if (_userPrograms.find(pRs.Id) == _userPrograms.cend() || pOverrideExitsCheck)
		{
			ShaderData* shaderDataPtr = dynamic_cast<ShaderData*>(pRs.RsData);
			std::string srcFS = File::LoadAsString(shaderDataPtr->SrcPath);
			std::string srcFSStart = File::LoadAsString("Rarium/ShaderToy.start.fs.glsl");
			std::string srcFSEnd = File::LoadAsString("Rarium/ShaderToy.end.fs.glsl");
			srcFS = srcFSStart + srcFS + srcFSEnd;

			Shader* FSSDPtr = new Shader(shaderDataPtr->SrcPath, srcFS);
			Program* ToyProgPtr = new Program(_shaders["Texture.vs"], FSSDPtr);
			ToyProgPtr->ExtractData();
			_userShaders[pRs.Id] = FSSDPtr;
			_userPrograms[pRs.Id] = ToyProgPtr;
		}
	}

	void Renderer::DeleteUiElementResources(UiElement* pUiElemPtr)
	{
		for (UiResource rs : pUiElemPtr->GetResources())
		{
			if (rs.IsDynamic)
			{
				switch (rs.Type)
				{
				case UiResourceType::Texture:
					delete _userTextures[rs.Id];
					_userTextures[rs.Id] = nullptr;
					break;
				case UiResourceType::Text:
					delete _userTexts[rs.Id];
					_userTexts[rs.Id] = nullptr;
					break;
				case UiResourceType::UserShader:
					delete _userPrograms[rs.Id];
					delete _userShaders[rs.Id];
					_userPrograms[rs.Id] = nullptr;
					_userShaders[rs.Id] = nullptr;
					break;
				default:
					break;
				}
			}
		}
	}
}