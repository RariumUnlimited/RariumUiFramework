//License : Please see Rarium.LICENSE
#ifndef _RariumProgram_

#define _RariumProgram_

#include <Rarium/Render/Shader.h>
#include <Rarium/Core/Rarium.h>
#include <vector>
#include <map>

namespace Rarium
{
	/**
	*	Information about a shader attribute
	*/
	struct Rarium_API AttributeInfo
	{
	public:
		std::string Name;///< Name of the attribute
		GLenum Type;///< Type of the attribute
		GLsizei Size;///< Size in byte of the attribute
		GLint Location;///< Location of the attribute

		/// Create an attribute info
		AttributeInfo(std::string pName, GLenum pType, GLint pLocation, GLsizei pSize)
		{
			Name = pName;
			Type = pType;
			Location = pLocation;
			Size = pSize;
		}

		AttributeInfo()
		{

		}
	};

	/**
	*	Information about a variable in a shader buffer
	*/
	struct Rarium_API BufferVariableInfo
	{
		std::string Name;///< Name of the variable
		GLint Offset;///< Offset of the variable
		GLenum Type;///< Type of the variable
		GLint ArrayStride;///< Array Stride of the variable
		GLint MatrixStride;///< Matrix Stride of the variable
		GLboolean IsRowMajor;///< If the matrix is in row major
		GLint TopLevelArrayStide;///< Top level array stride of the variable
		
		/// Create a new buffer variable info
		BufferVariableInfo(std::string pName, GLint pOffset, GLenum pType, GLint pArrayStride,
			GLint pMatrixStride, GLboolean pIsRowMajor, GLint pTopLevelArrayStide)
		{
			Name = pName;
			Offset = pOffset;
			Type = pType;
			ArrayStride = pArrayStride;
			MatrixStride = pMatrixStride;
			IsRowMajor = pIsRowMajor;
			TopLevelArrayStide = pTopLevelArrayStide;
		}

		BufferVariableInfo()
		{

		}
	};

	/**
	*	Information about a buffer in a shader
	*/
	struct Rarium_API BufferInfo
	{
		std::string Name;///< Name of the buffer
		GLint Index;///< Index of the buffer
		std::vector<BufferVariableInfo> Variables;///< List of all variable of the buffer

		/// Create a new buffer info
		BufferInfo(std::string pName, GLint pIndex, std::vector<BufferVariableInfo> pVariables)
		{
			Name = pName;
			Index = pIndex;
			Variables = pVariables;
		}

		BufferInfo()
		{

		}
	};

	/**
	* An OpenGL program pipeline
	*/
	class Rarium_API Program
	{
	public:
		/**
		*	Create a new OpenGL program
		*	@param pVertexShader Vertex shader of the program
		*	@param pFragmentShader Fragment shader of the program
		*/
		Program(Shader* pVertexShader, Shader* pFragmentShader);
		/**
		*	Destroy the program
		*/
		~Program();

		///Get the OpenGL name of the program
		inline GLuint GetProgram() { return _program; }

		/// Get the input of this shader
		inline std::map<std::string, AttributeInfo> GetInputs() { return _inputs; }
		/// Get the output of this shader
		inline std::map<std::string, AttributeInfo> GetOutputs() { return _outputs; }
		/// Get the uniform of this shader
		inline std::map<std::string, AttributeInfo> GetUniforms() { return _uniforms; }
		/// Get the buffer used by this shader
		inline std::map<std::string, BufferInfo> GetBuffers() { return _buffers; }

		/// Check if a given input is active in the program
		inline bool IsInputActive(std::string pInput) { return _inputs.find(pInput) != _inputs.cend(); }
		/// Check if a given output is active in the program
		inline bool IsOutputActive(std::string pOutput) { return _outputs.find(pOutput) != _outputs.cend(); }
		/// Check if a uniform is active in the program
		inline bool IsUniformActive(std::string pUniform) { return _uniforms.find(pUniform) != _uniforms.cend(); }
		/// Check if a buffer is active in the program
		inline bool IsBufferActive(std::string pBuffer) { return _buffers.find(pBuffer) != _buffers.cend(); }
		
		/**
		*	Extract data (such as uniform, input,...) from the shader
		*/
		void ExtractData();

	protected:
		Shader* _vertexPtr;///< Vertex shader of the program
		Shader* _fragmentPtr;///< Fragment shader of the program
		GLuint _program;///< OpenGL name of the program

		std::map<std::string,AttributeInfo> _inputs;///< Input of the shader
		std::map<std::string,AttributeInfo> _outputs;///< Output of the shader
		std::map<std::string,AttributeInfo> _uniforms;///< Uniform of the shader
		std::map<std::string,BufferInfo> _buffers;///< Buffer used by the shader

		/**
		*	Extract the attribute of the program
		*/
		void ExtractAttributeData();
		/**
		*	Extract Shader Storage Buffer data from the shader
		*/
		void ExtractStorageBlockData();
		/**
		*	Extract Attribute data from the shader
		*/
		void ExtractUniformData();
	};
}

#endif