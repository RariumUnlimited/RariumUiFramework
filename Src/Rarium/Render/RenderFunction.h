//License : Please see Rarium.LICENSE
#ifndef _RariumRenderFunction_

#define _RariumRenderFunction_

#include <Rarium/Core/Rarium.h>
#include <glbinding/gl/gl.h>
#include <string>
#include <glm/glm.hpp>
#include <Rarium/Ui/UiResourceType.h>

using namespace glm;

namespace Rarium
{
	class Rarium_API RenderFunction
	{
	public:
		/**
		*	Draw an image on screen
		*	@param pPath Path to the image
		*	@param pPosition Position on screen of the size
		*/
		virtual void DrawImage(std::string pPath, vec2 pPosition) = 0;
		/**
		*	Draw an image on screen while resizing it
		*	@param pPath Path to the image
		*	@param pPosition Position of the image on screen
		*	@param pSize New size of the image
		*/
		virtual void DrawImage(std::string pPath, vec2 pPosition, vec2 pSize) = 0;
		/**
		*	Draw an image on screen using the GPU handle of the image
		*	@param pHandle GPU Handle of the image
		*	@param pPosition Position of the image on screen
		*	@param pSize New size of the image
		*/
		virtual void DrawImage(gl::GLuint64 pHandle, vec2 pPosition, vec2 pSize) = 0;
		/**
		*	Draw a text on screen
		*	@param pPath Path of the text texture
		*	@param pPosition Position of the image on screen
		*/
		virtual void DrawText(std::string pPath, vec2 pPosition) = 0;
		/**
		*	Draw a shader toy widget on screen
		*	@param pShader Name of the shader to draw
		*	@param pPosition Position of the widget on screen
		*	@param pSize Size of the widget
		*	@param pTexture1 Optionnal texture to use in the shader
		*	@param pTexture2 Optionnal texture to use in the shader
		*	@param pTexture3 Optionnal texture to use in the shader
		*	@param pTexture4 Optionnal texture to use in the shader
		*	@param pTexture5 Optionnal texture to use in the shader
		*	@param pTexture6 Optionnal texture to use in the shader
		*/
		virtual void ShaderToy(std::string pShader, vec2 pPosition, vec2 pSize, std::string pTexture1 = "", std::string pTexture2 = "", std::string pTexture3 = "", std::string pTexture4 = "") = 0;
		/**
		*	Get the size of a resource
		*	@param pPath Path of the resource
		*	@param pRsType Type of the resource
		*	@return Size of the resource
		*/
		virtual vec2 GetResourceSize(std::string pPath, UiResourceType pRsType) = 0;
		/**
		*	Get the size of the display viewport
		*	@return Size of the display viewport
		*/
		virtual vec2 GetViewportSize() = 0;
	};
}

#endif