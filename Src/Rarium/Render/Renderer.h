//License : Please see Rarium.LICENSE
#ifndef _RariumRenderer_

#define _RariumRenderer_

#include <Rarium/Core/Rarium.h>
#include <glbinding/gl/gl.h>
#include <glbinding/Binding.h>
#include <Rarium/Render/GLUtil.h>
#include <Rarium/Core/Log.h>
#include <Rarium/Ui/Ui.h>
#include <Rarium/Render/Program.h>
#include <Rarium/Render/RenderFunction.h>
#include <Rarium/Render/Data/Texture.h>
#include <Rarium/Render/Data/Buffer.h>
#include <Rarium/Ui/UiElements/TextElement.h>
#include <Rarium/Ui/UIFunction.h>
#include <Rarium/Ui/UiDescriptor.h>
#include <map>

using namespace gl;

#define CheckGLError() Rarium::GLUtil::TreatGLError(ToString(__FILE__), ToString(__LINE__))

namespace Rarium
{
	/**
	*	Structure used to store data that can be used by all shaders
	*/
	struct Rarium_API GlobalData
	{
		vec2 ScreenResolution;///< Resolution of the viewport
	};

	namespace BlockBinding
	{
		enum Rarium_API BlockBinding
		{
			GlobalData = 0,
			Textures = 1
		};
	}

	class Rarium_API Renderer : public RenderFunction
	{
	public:
		/**
		*	Setup a ui to display by the renderer
		*	@param pUiPtr Pointer to the ui
		*/
		virtual void SetupUi(Ui* pUiPtr);
		/**
		*	Setup the menu to display by the renderer
		*	@param pMenuPtr Pointer to the menu
		*/
		void SetMenu(UiElement* pMenuPtr);

		/**
		*	Initialize the renderer
		*	@param width Width of the window
		*	@param height Height of the window
		*	@return True if the renderer successfully init
		*/
		virtual bool Init(unsigned int pWidth, unsigned int pHeight, UiFunction* pUiFunctionPtr);
		/**
		*	Draw a frame
		*	@param delta Delta time between the last frame and now
		*	@param pDisplayMenu If yes or no the renderer display the menu
		*/
		virtual void DrawFrame(float pDelta, bool pDisplayMenu);
		///Unload all resources that are on the GPU
		virtual void Unload();

		/// Treat an OpenGL error
		static void TreatGLError(std::string pFile, std::string pLine);
		/**
		*	Opengl Debug function
		*/
		static void GL_APIENTRY DebugProc(GLenum pSource, GLenum pType, GLuint pId, GLenum pSeverity, GLsizei pLength, const GLchar* pMessageAR, void* pUserParamPtr);

		GLDEBUGPROC debugProc;///< Function used when debugging OpenGL

		//===============<RenderFunction>====================
		virtual void DrawImage(std::string pPath, vec2 pPosition) override;
		virtual void DrawImage(std::string pPath, vec2 pPosition, vec2 pSize) override;
		virtual void DrawImage(GLuint64 pHandle, vec2 pPosition, vec2 pSize) override;
		virtual void DrawText(std::string pPath, vec2 pPosition) override;
		virtual void ShaderToy(std::string pShader, vec2 pPosition, vec2 pSize, std::string pTexture1 = "", std::string pTexture2 = "", std::string pTexture3 = "", std::string pTexture4 = "") override;
		virtual vec2 GetResourceSize(std::string pPath, UiResourceType pRsType) override;
		virtual vec2 GetViewportSize() override;
		//===============<RenderFunction>====================

	protected:
		UiFunction* _uiFunctionPtr;///< Pointer to an UiFunction object
		Ui* _uiPtr;///< Pointer to the current ui displayed
		UiElement* _menuPtr;///< Pointer to the menu displayed
		GLuint _baseVAO;///< Vertex Array Object used to draw ui element
		unsigned int _width;///< Screen width
		unsigned int _height;///< Screen height
		std::map<std::string, Program*> _programs;///< List of programs used by the renderer
		std::map<std::string, Shader*> _shaders;///< List of all shaders used by the renderer 

		//===============<Resources>====================
		std::map<std::string, Texture*> _userTextures;
		std::map<std::string, Texture*> _userTexts;
		std::map<std::string, Shader*> _userShaders;
		std::map<std::string, Program*> _userPrograms;
		//===============<Resources>====================

		std::vector<UiElement*> _dynamicElements;///< List of all ui elements that can be updated each frame

		std::vector<UiDescriptor*> _allDescriptors;///< List of all ui descriptor loaded
		std::vector<UiDescriptor*> _diplayedDescriptors;///< List of all descriptor of ui that can be displayed

		Buffer* _globalDataBufferPtr;///< Pointer to a buffer that contains the data of _globalData
		GlobalData _globalData;///< Global data of shaders

		/**
		*	Draw the ui on screen
		*	@param pDelta Delta time since last draw
		*/
		void DrawUi(float pDelta);
		/**
		*	Draw the menu
		*	@param pDelta Delta time since last draw
		*/
		void DrawMenu(float pDelta);

		/**
		*	Load the resources for a shader toy widget
		*	@param pRs Resource object for the shader toy widget
		*	@param pOverrideExitsCheck If true, override existing loaded resources
		*/
		void LoadShaderToyProgram(UiResource pRs, bool pOverrideExitsCheck = false);
		/**
		*	Load the resources for a text texture
		*	@param pRs Resource object for the text texture
		*	@param pOverrideExitsCheck If true, override existing loaded resources
		*/
		void LoadTextTexture(UiResource pRs, bool pOverrideExitsCheck = false);
		/**
		*	Process an ui element to load it
		*	@param pElemPtr Pointer to the ui element to load
		*/
		void ProcessElement(UiElement* pElemPtr);
		/**
		*	Update an already loaded dynamic ui element
		*	@param pElemPtr Pointer to the ui element to update
		*/
		void UpdateElement(UiElement* pElemPtr);
		///Update all dynamic ui element
		void UpdateDynamicElements();
		///Unload all resources loaded
		void UnloadAssets();

		/// Setup all programs that are used by the renderer
		void SetupPrograms();
		/// Setup all buffers used by shaders
		void SetupBuffers();

		/**
		*	Delete all loaded resources of a ui element
		*	@param pUiElemPtr Pointer to the ui element that we want resources deleted
		*/
		void DeleteUiElementResources(UiElement* pUiElemPtr);
	};
}

#endif