//License : Please see Rarium.LICENSE
#include <Rarium/Render/GLUtil.h>
#include <Rarium/Core/Log.h>
#include <Rarium/Core/Exception.h>
#include <map>
#include <glm/glm.hpp>

using namespace glm;

namespace Rarium
{
	void GLUtil::TreatGLError(std::string pFile, std::string pLine)
	{
		GLenum error = glGetError();

		switch (error)
		{
		case GL_NO_ERROR:
			break;
		case GL_INVALID_ENUM:
			Log("GL Error Occured : " + pFile + ", line : " + pLine);
			throw Exception("Received gl error : invalid enum - File : " + pFile + " Line : " + pLine);
			break;
		case GL_INVALID_VALUE:
			Log("GL Error Occured : " + pFile + ", line : " + pLine);
			throw Exception("Received gl error : invalid value - File:" + pFile + " Line:" + pLine);
			break;
		case GL_INVALID_OPERATION:
			Log("GL Error Occured : " + pFile + ", line : " + pLine);
			throw Exception("Received gl error : invalid operation - File : " + pFile + " Line : " + pLine);
			break;
		case GL_OUT_OF_MEMORY:
			Log("GL Error Occured : " + pFile + ", line : " + pLine);
			throw Exception("Received gl error : out of memory - File : " + pFile + " Line : " + pLine);
			break;
		default:
			Log("GL Error Occured : " + pFile + ", line : " + pLine);
			throw Exception("Received unknown opengl error");
			break;
		}
	}

	std::string GLUtil::SourceToStr(GLenum pSource)
	{
		static std::map<GLenum, std::string> sourceToStr = {
			{ GL_DEBUG_SOURCE_API, "API" },
			{ GL_DEBUG_SOURCE_SHADER_COMPILER, "Shader Compiler" },
			{ GL_DEBUG_SOURCE_WINDOW_SYSTEM, "Window System" },
			{ GL_DEBUG_SOURCE_THIRD_PARTY, "Third Party" },
			{ GL_DEBUG_SOURCE_APPLICATION, "Application" },
			{ GL_DEBUG_SOURCE_OTHER, "Other" } };

		return sourceToStr[pSource];
	}

	std::string GLUtil::TypeToStr(GLenum pType)
	{
		static std::map<GLenum, std::string> typeToStr = {
			{ GL_DEBUG_TYPE_ERROR, "Error" },
			{ GL_DEBUG_TYPE_DEPRECATED_BEHAVIOR, "Deprecated Behavior" },
			{ GL_DEBUG_TYPE_UNDEFINED_BEHAVIOR, "Undefined Behavior" },
			{ GL_DEBUG_TYPE_PERFORMANCE, "Performance" },
			{ GL_DEBUG_TYPE_PORTABILITY, "Portability" },
			{ GL_DEBUG_TYPE_MARKER, "Marker" },
			{ GL_DEBUG_TYPE_PUSH_GROUP, "Push Group" },
			{ GL_DEBUG_TYPE_POP_GROUP, "Pop Group" },
			{ GL_DEBUG_TYPE_OTHER, "Other" } };

		return typeToStr[pType];
	}

	std::string GLUtil::SeverityToStr(GLenum pSeverity)
	{
		static std::map<GLenum, std::string> severityToStr = {
			{ GL_DEBUG_SEVERITY_LOW, "Low" },
			{ GL_DEBUG_SEVERITY_MEDIUM, "Medium" },
			{ GL_DEBUG_SEVERITY_HIGH, "High" },
			{ GL_DEBUG_SEVERITY_NOTIFICATION, "Notification" } };

		return severityToStr[pSeverity];
	}

	std::string GLUtil::GLSLTypeToStr(GLenum pType)
	{
		static std::map<GLenum, std::string> typeToStr = {
			{ GL_FLOAT, "float" },
			{ GL_FLOAT_VEC2, "vec2" },
			{ GL_FLOAT_VEC3, "vec3" },
			{ GL_FLOAT_VEC4, "vec4" },
			{ GL_DOUBLE, "double" },
			{ GL_DOUBLE_VEC2, "dvec2" },
			{ GL_DOUBLE_VEC3, "dvec3" },
			{ GL_DOUBLE_VEC4, "dvec4" },
			{ GL_INT, "int" },
			{ GL_INT_VEC2, "ivec2" },
			{ GL_INT_VEC3, "ivec3" },
			{ GL_INT_VEC4, "ivec4" },
			{ GL_UNSIGNED_INT, "unsigned int" },
			{ GL_UNSIGNED_INT_VEC2, "uvec2" },
			{ GL_UNSIGNED_INT_VEC3, "uvec3" },
			{ GL_UNSIGNED_INT_VEC4, "uvec4" },
			{ GL_BOOL, "bool" },
			{ GL_BOOL_VEC2, "bvec2" },
			{ GL_BOOL_VEC3, "bvec3" },
			{ GL_BOOL_VEC4, "bvec4" },
			{ GL_FLOAT_MAT2, "mat2" },
			{ GL_FLOAT_MAT3, "mat3" },
			{ GL_FLOAT_MAT4, "mat4" },
			{ GL_FLOAT_MAT2x3, "mat2x3" },
			{ GL_FLOAT_MAT2x4, "mat2x4" },
			{ GL_FLOAT_MAT3x2, "mat3x2" },
			{ GL_FLOAT_MAT3x4, "mat3x4" },
			{ GL_FLOAT_MAT4x2, "mat4x2" },
			{ GL_FLOAT_MAT4x3, "mat4x3" },
			{ GL_DOUBLE_MAT2, "dmat2" },
			{ GL_DOUBLE_MAT3, "dmat3" },
			{ GL_DOUBLE_MAT4, "dmat4" },
			{ GL_DOUBLE_MAT2x3, "dmat2x3" },
			{ GL_DOUBLE_MAT2x4, "dmat2x4" },
			{ GL_DOUBLE_MAT3x2, "dmat3x2" },
			{ GL_DOUBLE_MAT3x4, "dmat3x4" },
			{ GL_DOUBLE_MAT4x2, "dmat4x2" },
			{ GL_DOUBLE_MAT4x3, "dmat4x3" },
			{ GL_SAMPLER_1D, "sampler1D" },
			{ GL_SAMPLER_2D, "sampler2D" },
			{ GL_SAMPLER_3D, "sampler3D" },
			{ GL_SAMPLER_CUBE, "samplerCube" },
			{ GL_SAMPLER_1D_SHADOW, "sampler1DShadow" },
			{ GL_SAMPLER_2D_SHADOW, "sampler2DShadow" },
			{ GL_SAMPLER_1D_ARRAY, "sampler1DArray" },
			{ GL_SAMPLER_2D_ARRAY, "sampler2DArray" },
			{ GL_SAMPLER_1D_ARRAY_SHADOW, "sampler1DArrayShadow" },
			{ GL_SAMPLER_2D_ARRAY_SHADOW, "sampler2DArrayShadow" },
			{ GL_SAMPLER_2D_MULTISAMPLE, "sampler2DMS" },
			{ GL_SAMPLER_2D_MULTISAMPLE_ARRAY, "sampler2DMSArray" },
			{ GL_SAMPLER_CUBE_SHADOW, "samplerCubeShadow" },
			{ GL_SAMPLER_BUFFER, "samplerBuffer" },
			{ GL_SAMPLER_2D_RECT, "sampler2DRect" },
			{ GL_SAMPLER_2D_RECT_SHADOW, "sampler2DRectShadow" },
			{ GL_INT_SAMPLER_1D, "isampler1D" },
			{ GL_INT_SAMPLER_2D, "isampler2D" },
			{ GL_INT_SAMPLER_3D, "isampler3D" },
			{ GL_INT_SAMPLER_CUBE, "isamplerCube" },
			{ GL_INT_SAMPLER_1D_ARRAY, "isampler1DArray" },
			{ GL_INT_SAMPLER_2D_ARRAY, "isampler2DArray" },
			{ GL_INT_SAMPLER_2D_MULTISAMPLE, "isampler2DMS" },
			{ GL_INT_SAMPLER_2D_MULTISAMPLE_ARRAY, "isampler2DMSArray" },
			{ GL_INT_SAMPLER_BUFFER, "isamplerBuffer" },
			{ GL_INT_SAMPLER_2D_RECT, "isampler2DRect" },
			{ GL_UNSIGNED_INT_SAMPLER_1D, "usampler1D" },
			{ GL_UNSIGNED_INT_SAMPLER_2D, "usampler2D" },
			{ GL_UNSIGNED_INT_SAMPLER_3D, "usampler3D" },
			{ GL_UNSIGNED_INT_SAMPLER_CUBE, "usamplerCube" },
			{ GL_UNSIGNED_INT_SAMPLER_1D_ARRAY, "usampler2DArray" },
			{ GL_UNSIGNED_INT_SAMPLER_2D_ARRAY, "usampler2DArray" },
			{ GL_UNSIGNED_INT_SAMPLER_2D_MULTISAMPLE, "usampler2DMS" },
			{ GL_UNSIGNED_INT_SAMPLER_2D_MULTISAMPLE_ARRAY, "usampler2DMSArray" },
			{ GL_UNSIGNED_INT_SAMPLER_BUFFER, "usamplerBuffer" },
			{ GL_UNSIGNED_INT_SAMPLER_2D_RECT, "usampler2DRect" } };

		return typeToStr[pType];
	}

	GLsizei GLUtil::SizeofGLSLType(GLenum pType)
	{
		static std::map<GLenum, GLsizei> typeToSize = {
			{ GL_FLOAT, sizeof(float) },
			{ GL_FLOAT_VEC2, sizeof(vec2) },
			{ GL_FLOAT_VEC3, sizeof(vec3) },
			{ GL_FLOAT_VEC4, sizeof(vec4) },
			{ GL_DOUBLE, sizeof(double) },
			{ GL_DOUBLE_VEC2, sizeof(dvec2) },
			{ GL_DOUBLE_VEC3, sizeof(dvec3) },
			{ GL_DOUBLE_VEC4, sizeof(dvec4) },
			{ GL_INT, sizeof(int) },
			{ GL_INT_VEC2, sizeof(ivec2) },
			{ GL_INT_VEC3, sizeof(ivec3) },
			{ GL_INT_VEC4, sizeof(ivec4) },
			{ GL_UNSIGNED_INT, sizeof(unsigned int) },
			{ GL_UNSIGNED_INT_VEC2, sizeof(uvec2) },
			{ GL_UNSIGNED_INT_VEC3, sizeof(uvec3) },
			{ GL_UNSIGNED_INT_VEC4, sizeof(uvec4) },
			{ GL_BOOL, sizeof(bool) },
			{ GL_BOOL_VEC2, sizeof(bvec2) },
			{ GL_BOOL_VEC3, sizeof(bvec3) },
			{ GL_BOOL_VEC4, sizeof(bvec4) },
			{ GL_FLOAT_MAT2, sizeof(mat2) },
			{ GL_FLOAT_MAT3, sizeof(mat3) },
			{ GL_FLOAT_MAT4, sizeof(mat4) },
			{ GL_FLOAT_MAT2x3, sizeof(mat2x3) },
			{ GL_FLOAT_MAT2x4, sizeof(mat2x4) },
			{ GL_FLOAT_MAT3x2, sizeof(mat3x2) },
			{ GL_FLOAT_MAT3x4, sizeof(mat3x4) },
			{ GL_FLOAT_MAT4x2, sizeof(mat4x2) },
			{ GL_FLOAT_MAT4x3, sizeof(mat4x3) },
			{ GL_DOUBLE_MAT2, sizeof(dmat2) },
			{ GL_DOUBLE_MAT3, sizeof(dmat3) },
			{ GL_DOUBLE_MAT4, sizeof(dmat4) },
			{ GL_DOUBLE_MAT2x3, sizeof(dmat2x3) },
			{ GL_DOUBLE_MAT2x4, sizeof(dmat2x4) },
			{ GL_DOUBLE_MAT3x2, sizeof(dmat3x2) },
			{ GL_DOUBLE_MAT3x4, sizeof(dmat3x4) },
			{ GL_DOUBLE_MAT4x2, sizeof(dmat4x2) },
			{ GL_DOUBLE_MAT4x3, sizeof(dmat4x3) } };

		return typeToSize[pType];
	}

	GLenum GLUtil::GLSLTypeToBaseType(GLenum pType)
	{
		static std::map<GLenum, GLenum> typeToBase = {
			{ GL_FLOAT, GL_FLOAT },
			{ GL_FLOAT_VEC2, GL_FLOAT },
			{ GL_FLOAT_VEC3, GL_FLOAT },
			{ GL_FLOAT_VEC4, GL_FLOAT },
			{ GL_DOUBLE, GL_DOUBLE },
			{ GL_DOUBLE_VEC2, GL_DOUBLE },
			{ GL_DOUBLE_VEC3, GL_DOUBLE },
			{ GL_DOUBLE_VEC4, GL_DOUBLE },
			{ GL_INT, GL_INT },
			{ GL_INT_VEC2, GL_INT },
			{ GL_INT_VEC3, GL_INT },
			{ GL_INT_VEC4, GL_INT },
			{ GL_UNSIGNED_INT, GL_UNSIGNED_INT },
			{ GL_UNSIGNED_INT_VEC2, GL_UNSIGNED_INT },
			{ GL_UNSIGNED_INT_VEC3, GL_UNSIGNED_INT },
			{ GL_UNSIGNED_INT_VEC4, GL_UNSIGNED_INT },
			{ GL_BOOL, GL_BOOL },
			{ GL_BOOL_VEC2, GL_BOOL },
			{ GL_BOOL_VEC3, GL_BOOL },
			{ GL_BOOL_VEC4, GL_BOOL },
			{ GL_FLOAT_MAT2, GL_FLOAT },
			{ GL_FLOAT_MAT3, GL_FLOAT },
			{ GL_FLOAT_MAT4, GL_FLOAT },
			{ GL_FLOAT_MAT2x3, GL_FLOAT },
			{ GL_FLOAT_MAT2x4, GL_FLOAT },
			{ GL_FLOAT_MAT3x2, GL_FLOAT },
			{ GL_FLOAT_MAT3x4, GL_FLOAT },
			{ GL_FLOAT_MAT4x2, GL_FLOAT },
			{ GL_FLOAT_MAT4x3, GL_FLOAT },
			{ GL_DOUBLE_MAT2, GL_DOUBLE },
			{ GL_DOUBLE_MAT3, GL_DOUBLE },
			{ GL_DOUBLE_MAT4, GL_DOUBLE },
			{ GL_DOUBLE_MAT2x3, GL_DOUBLE },
			{ GL_DOUBLE_MAT2x4, GL_DOUBLE },
			{ GL_DOUBLE_MAT3x2, GL_DOUBLE },
			{ GL_DOUBLE_MAT3x4, GL_DOUBLE },
			{ GL_DOUBLE_MAT4x2, GL_DOUBLE },
			{ GL_DOUBLE_MAT4x3, GL_DOUBLE } };

		return typeToBase[pType];
	}

	GLuint GLUtil::GLSLTypeToComponentCount(GLenum pType)
	{
		static std::map<GLenum, GLuint> typeToComp = {
			{ GL_FLOAT, 1 },
			{ GL_FLOAT_VEC2, 2 },
			{ GL_FLOAT_VEC3, 3 },
			{ GL_FLOAT_VEC4, 4 },
			{ GL_DOUBLE, 1 },
			{ GL_DOUBLE_VEC2, 2 },
			{ GL_DOUBLE_VEC3, 3 },
			{ GL_DOUBLE_VEC4, 4 },
			{ GL_INT, 1 },
			{ GL_INT_VEC2, 2 },
			{ GL_INT_VEC3, 3 },
			{ GL_INT_VEC4, 4 },
			{ GL_UNSIGNED_INT, 1 },
			{ GL_UNSIGNED_INT_VEC2, 2 },
			{ GL_UNSIGNED_INT_VEC3, 3 },
			{ GL_UNSIGNED_INT_VEC4, 4 },
			{ GL_BOOL, 1 },
			{ GL_BOOL_VEC2, 2 },
			{ GL_BOOL_VEC3, 3 },
			{ GL_BOOL_VEC4, 4 } };

		return typeToComp[pType];
	}
}