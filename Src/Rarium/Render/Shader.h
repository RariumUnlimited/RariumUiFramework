//License : Please see Rarium.LICENSE
#ifndef _RariumShader_

#define _RariumShader_

#include <Rarium/Core/Rarium.h>
#include <glbinding/gl/gl.h>

using namespace gl;

namespace Rarium
{
	/**
	*	An OpenGL Shader
	*/
	class Rarium_API Shader
	{
	public:
		/**
		*	Create a new shader on the gpu
		*	@param name Name of the asset
		*	@param data Data of the texture file
		*	@param size Size of data
		*/
		Shader(std::string pName, std::string pSrc);
		/**
		*	Destroy our shader
		*/
		~Shader();

		/// Get the separate program of this shader
		inline GLuint GetShader() { return _shader; }
		/// Get the type of this shader
		inline GLenum GetType() { return _type; }
		/// Get the name of the shader
		inline std::string GetName() { return _name; }

	protected:
		std::string _name;///< Name of the shader
		GLuint _shader;///< Separate program of the shader on the GPU
		GLenum _type;///< Type of the shader
	};
}

#endif