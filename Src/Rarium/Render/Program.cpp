//License : Please see Rarium.LICENSE
#include <Rarium/Render/Program.h>
#include <Rarium/Core/Log.h>
#include <Rarium/Core/Exception.h>
#include <Rarium/Render/GLUtil.h>

namespace Rarium
{
	Program::Program(Shader* pVertexShader, Shader* pFragmentShader)
	{
		_vertexPtr = pVertexShader;
		_fragmentPtr = pFragmentShader;
		GLint compileStatus = 0;
		GLint logSize = 0;
		GLsizei length = 0;
		GLchar* logAr;

		_program = glCreateProgram();
		glAttachShader(_program, _vertexPtr->GetShader());
		glAttachShader(_program, _fragmentPtr->GetShader());
		glLinkProgram(_program);


		glGetProgramiv(_program, GL_LINK_STATUS, &compileStatus);

		if (compileStatus != (GLint)GL_TRUE)
		{
			glGetProgramiv(_program, GL_INFO_LOG_LENGTH, &logSize);

			logAr = new GLchar[logSize];
			glGetProgramInfoLog(_program, logSize, &length, logAr);
			Log("Shaders " + _vertexPtr->GetName() + " + " + _fragmentPtr->GetName() + " link log : ");
			Log(ToString(logAr));
			delete[] logAr;

			throw Exception("Shaders " + _vertexPtr->GetName() + " + " + _fragmentPtr->GetName() + " did not link, see log above");
		}

	}

	Program::~Program()
	{
		glDeleteProgram(_program);
	}

	void Program::ExtractData()
	{
		Log("Shaders " + _vertexPtr->GetName() + " + " + _fragmentPtr->GetName())
		ExtractAttributeData();
		ExtractStorageBlockData();
		ExtractUniformData();
	}

	void Program::ExtractUniformData()
	{
		Log("\tUniform : ");
		char tempNameAR[1024];
		GLint numUni = 0;
		glGetProgramInterfaceiv(_program, GL_UNIFORM, GL_ACTIVE_RESOURCES, &numUni);
		const GLenum propertiesAR[3] = { GL_TYPE, GL_LOCATION, GL_BLOCK_INDEX };

		for (int uni = 0; uni < numUni; uni++)
		{
			GLint valuesAR[3];
			glGetProgramResourceiv(_program, GL_UNIFORM, uni, 3, propertiesAR, 2, NULL, valuesAR);

			glGetProgramResourceName(_program, GL_UNIFORM, uni, 1024, NULL, tempNameAR);

			std::string uniName(tempNameAR);

			_uniforms.insert(std::make_pair(uniName,AttributeInfo(uniName, GLenum(valuesAR[0]), valuesAR[1], GLUtil::SizeofGLSLType(GLenum(valuesAR[0])))));
			Log("\t\t" + uniName + " " + ToString(valuesAR[1]) + " " + GLUtil::GLSLTypeToStr(GLenum(valuesAR[0])));
		
		}
	}

	void Program::ExtractStorageBlockData()
	{
		GLint numBlocks = 0;
		glGetProgramInterfaceiv(_program, GL_SHADER_STORAGE_BLOCK, GL_ACTIVE_RESOURCES, &numBlocks);

		char tempNameAR[1024];

		Log("\tBuffer :")

			for (int blockIdx = 0; blockIdx < numBlocks; blockIdx++)
			{
				std::vector<BufferVariableInfo> vars;

				glGetProgramResourceName(_program, GL_SHADER_STORAGE_BLOCK, blockIdx, 1024, NULL, tempNameAR);

				std::string bufferName(tempNameAR);

				GLint binding = 0;
				{
					GLenum prop[] = { GL_BUFFER_BINDING };
					glGetProgramResourceiv(_program, GL_SHADER_STORAGE_BLOCK, blockIdx, 1, prop, 1, NULL, &binding);
				}

				Log("\t\t" + bufferName + " index " + ToString(blockIdx) + " binding " + ToString(binding));

				GLint vcount = 0;
				{
					GLenum prop[] = { GL_NUM_ACTIVE_VARIABLES };
					glGetProgramResourceiv(_program, GL_SHADER_STORAGE_BLOCK, blockIdx, 1, prop, 1, NULL, &vcount);
				}

				std::vector<GLint> variables(vcount);
				{
					GLenum prop[] = { GL_ACTIVE_VARIABLES };
					glGetProgramResourceiv(_program, GL_SHADER_STORAGE_BLOCK, blockIdx, 1, prop, vcount, NULL, &variables.front());
				}

				GLchar vnameAR[128] = { 0 };
				for (int k = 0; k < vcount; k++)
				{
					GLenum propsAR[] = { GL_OFFSET, GL_TYPE, GL_ARRAY_STRIDE, GL_MATRIX_STRIDE, GL_IS_ROW_MAJOR, GL_TOP_LEVEL_ARRAY_STRIDE };
					GLint paramsAR[sizeof(propsAR) / sizeof(GLenum)];

					glGetProgramResourceiv(_program, GL_BUFFER_VARIABLE, variables[k], sizeof(propsAR) / sizeof(GLenum), propsAR, sizeof(paramsAR) / sizeof(GLenum), NULL, paramsAR);
					glGetProgramResourceName(_program, GL_BUFFER_VARIABLE, variables[k], sizeof(vnameAR), NULL, vnameAR);

					Log("\t\t\t" + std::string(vnameAR) + " offset " + ToString(paramsAR[0]) + " type " + GLUtil::GLSLTypeToStr(GLenum(paramsAR[1])) + " array stride " + ToString(paramsAR[2]) + " top level stride " + ToString(paramsAR[5]));

					vars.push_back(BufferVariableInfo(std::string(vnameAR), paramsAR[0], GLenum(paramsAR[1]), paramsAR[2], paramsAR[3], GLboolean(paramsAR[4]), paramsAR[5]));
				}

				_buffers.insert(std::make_pair(bufferName, BufferInfo(bufferName, blockIdx, vars)));
			}
	}

	void Program::ExtractAttributeData()
	{
		Log("\tInput :");
		char tempNameAR[1024];

		{
			GLint numInput = 0;
			glGetProgramInterfaceiv(_program, GL_PROGRAM_INPUT, GL_ACTIVE_RESOURCES, &numInput);
			const GLenum propertiesAR[2] = { GL_TYPE, GL_LOCATION };

			for (int input = 0; input < numInput; input++)
			{
				GLint valuesAR[2];
				glGetProgramResourceiv(_program, GL_PROGRAM_INPUT, input, 2, propertiesAR, 2, NULL, valuesAR);

				glGetProgramResourceName(_program, GL_PROGRAM_INPUT, input, 1024, NULL, tempNameAR);

				std::string inputName(tempNameAR);

				if (inputName.size() >= 3 && inputName.substr(0, 3) == "gl_")
					continue;
				else
				{
					_inputs.insert(std::make_pair(inputName, AttributeInfo(inputName, GLenum(valuesAR[0]), valuesAR[1], GLUtil::SizeofGLSLType(GLenum(valuesAR[0])))));
					Log("\t\t" + inputName + " " + ToString(valuesAR[1]) + " " + GLUtil::GLSLTypeToStr(GLenum(valuesAR[0])));
				}
			}
		}

		Log("\tOutput :");
		{
			GLint numOutput = 0;
			glGetProgramInterfaceiv(_program, GL_PROGRAM_OUTPUT, GL_ACTIVE_RESOURCES, &numOutput);
			const GLenum propertiesAR[2] = { GL_TYPE, GL_LOCATION };

			for (int outpout = 0; outpout < numOutput; outpout++)
			{
				GLint valuesAR[2];
				glGetProgramResourceiv(_program, GL_PROGRAM_OUTPUT, outpout, 2, propertiesAR, 2, NULL, valuesAR);

				glGetProgramResourceName(_program, GL_PROGRAM_OUTPUT, outpout, 1024, NULL, tempNameAR);

				std::string outputName(tempNameAR);

				if (outputName.size() >= 3 && outputName.substr(0, 3) != "gl_")
				{
					_outputs.insert(std::make_pair(outputName, AttributeInfo(outputName, GLenum(valuesAR[0]), valuesAR[1], GLUtil::SizeofGLSLType(GLenum(valuesAR[0])))));
					Log("\t\t" + outputName + " " + ToString(valuesAR[1]) + " " + GLUtil::GLSLTypeToStr(GLenum(valuesAR[0])));
				}
			}
		}
	}
}