//License : Please see Rarium.LICENSE
#ifndef _RariumBuffer_

#define _RariumBuffer_

#include <glbinding/gl/gl.h>
#include <Rarium/Core/Rarium.h>

using namespace gl;
using namespace glbinding;

namespace Rarium
{
	/**
	*	A static OpenGL buffer
	*/
	class Rarium_API Buffer
	{
	public:
		/**
		*	Build a new buffer
		*	@param pSize size in byte of the buffer
		*	@param pLabel A label for the buffer
		*/
		Buffer(GLsizei pSize, std::string pLabel = "");
		/**
		*	Destroy the buffer
		*/
		~Buffer();
		/**
		*	Update the buffer with new data
		*	@param pDataPtr Pointer to the new data
		*	@param pDataSize Size in byte of the new data
		*/
		void Update(void* pDataPtr, GLsizei pDataSize);

		/// Get the OpenGL name of the buffer
		inline GLuint GetName() { return _name; }
		/// Get a pointer to the buffer mapped in memory
		inline void* GetMapPtr() { return _mapPtr; }
		/// Get the label of the buffer
		inline std::string GetLabel() { return _label; }
		/// Get the size in byte of the buffer
		inline GLsizei GetSize() { return _size; }

		/// Change the label of the buffer
		inline void SetLabel(std::string pLabel) { _label = pLabel; }

	protected:
		GLuint _name;///< OpenGL name of the buffer
		void* _mapPtr;///< Point to the buffer mapped in memory
		std::string _label;///< Label of the buffer
		GLsizei _size;///< Size in byte of the buffer
	};
}

#endif