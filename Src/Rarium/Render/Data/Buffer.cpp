//License : Please see Rarium.LICENSE
#include <Rarium/Render/Data/Buffer.h>
#include <Rarium/Render/Renderer.h>

namespace Rarium
{
	Buffer::Buffer(GLsizei pSize, std::string pLabel)
	{
		_size = pSize;
		if (_size > 0)
		{
			glCreateBuffers(1, &_name);
			glNamedBufferStorage(_name, _size, nullptr, GL_MAP_READ_BIT | GL_MAP_WRITE_BIT | GL_MAP_PERSISTENT_BIT);
			_mapPtr = glMapNamedBufferRange(_name, 0, _size, GL_MAP_READ_BIT | GL_MAP_WRITE_BIT | GL_MAP_PERSISTENT_BIT);

			_label = pLabel;

			CheckGLError();
		}
	}

	Buffer::~Buffer()
	{
		glDeleteBuffers(1, &_name);
	}

	void Buffer::Update(void* pDataPtr, GLsizei pDataSize)
	{
		if (_size > 0)
		{
			GLsizei sizeToUpdate = std::min(pDataSize, _size);
			memcpy(_mapPtr, pDataPtr, sizeToUpdate);
		}
	}
}