//License : Please see Rarium.LICENSE
#include <Rarium/Render/Data/Texture.h>
#include <Rarium/Core/File.h>
#include <SDL2/SDL_image.h>
#include <SDL2/SDL_ttf.h>
#include <Rarium/Core/Exception.h>
#include <gli/gli.hpp>
#include <gli/load.hpp>
#include <gli/save.hpp>
#include <gli/gl.hpp>

#include <Rarium/Render/Renderer.h>

namespace Rarium
{
	Texture::Texture(std::string pPath)
	{
		_path = pPath;
		
		if (pPath.find(".dds") != std::string::npos)
		{
			CreateTexture(std::string(File::GetBasePath() + _path).c_str());
		}
		else
		{
			SDL_Surface* surfacePtr = IMG_Load(std::string(File::GetBasePath() + _path).c_str());

			if (surfacePtr != nullptr)
			{
				SDL_Surface* convertedPtr = SDL_ConvertSurfaceFormat(surfacePtr, SDL_PIXELFORMAT_RGBA8888, 0);

				convertedPtr = FlipHorizontally(convertedPtr);

				LoadUpSDLTexture(convertedPtr);

				SDL_FreeSurface(convertedPtr);
				SDL_FreeSurface(surfacePtr);
			}
			else
			{
				throw Exception("Unable to load texture : " + std::string(File::GetBasePath() + _path));
			}
		}
	}

	Texture::Texture(std::string pText, std::string pFont, unsigned int pFontSize, vec3 pColor)
	{
		TTF_Font* font = NULL;
		font = TTF_OpenFont(std::string(File::GetBasePath() + pFont).c_str(), pFontSize);

		SDL_Color colorSDL = { Uint8(255 * pColor.x), Uint8(255 * pColor.y), Uint8(255 * pColor.z) };

		SDL_Surface* textSurface = TTF_RenderText_Blended(font, pText.c_str(), colorSDL);

		if(textSurface == nullptr)
			textSurface = SDL_CreateRGBSurface(0, 1, 1, 32, 0, 0, 0, 0);

		SDL_Surface* converted = SDL_ConvertSurfaceFormat(textSurface, SDL_PIXELFORMAT_RGBA8888, 0);

		converted = FlipHorizontally(converted);

		LoadUpSDLTexture(converted);

		SDL_FreeSurface(converted);
		SDL_FreeSurface(textSurface);

		TTF_CloseFont(font);
	}

	Texture::~Texture()
	{
		glDeleteTextures(1, &_name);
	}

	void Texture::CreateTexture(const char* pFilename)
	{
		gli::texture Texture = gli::load(pFilename);

		if (Texture.empty())
			throw Exception("Texture is empty : " + std::string(pFilename));
		gli::gl GL;
		gli::gl::format const Format = GL.translate(Texture.format());
		GLenum Target = GLenum(GL.translate(Texture.target()));
		GLuint TextureName = 0;
		glGenTextures(1, &TextureName);
		glBindTexture(Target, TextureName);
		/*glTexParameteri(Target, GL_TEXTURE_BASE_LEVEL, 0);
		glTexParameteri(Target, GL_TEXTURE_MAX_LEVEL, static_cast<GLint>(Texture.levels() - 1));
		glTexParameteri(Target, GL_TEXTURE_SWIZZLE_R, Format.Swizzles.r);
		glTexParameteri(Target, GL_TEXTURE_SWIZZLE_G, Format.Swizzles.g);
		glTexParameteri(Target, GL_TEXTURE_SWIZZLE_B, Format.Swizzles.b);
		glTexParameteri(Target, GL_TEXTURE_SWIZZLE_A, Format.Swizzles.a);*/
		glTexParameteri(Target, GL_TEXTURE_MIN_FILTER, (GLint)GL_LINEAR);
		glTexParameteri(Target, GL_TEXTURE_MAG_FILTER, (GLint)GL_LINEAR);

		glm::tvec3<GLsizei> const Dimensions(Texture.dimensions());
		GLsizei const FaceTotal = static_cast<GLsizei>(Texture.layers() * Texture.faces());
		switch (Texture.target())
		{
		case gli::TARGET_1D:
			glTexStorage1D(Target, static_cast<GLint>(Texture.levels()), GLenum(Format.Internal), Dimensions.x);
			break;
		case gli::TARGET_1D_ARRAY:
		case gli::TARGET_2D:
		case gli::TARGET_CUBE:
			glTexStorage2D(
				Target, static_cast<GLint>(Texture.levels()), GLenum(Format.Internal),
				Dimensions.x, Texture.target() == gli::TARGET_2D ? Dimensions.y : FaceTotal);
			break;
		case gli::TARGET_2D_ARRAY:
		case gli::TARGET_3D:
		case gli::TARGET_CUBE_ARRAY:
			glTexStorage3D(
				Target, static_cast<GLint>(Texture.levels()), GLenum(Format.Internal),
				Dimensions.x, Dimensions.y, Texture.target() == gli::TARGET_3D ? Dimensions.z : FaceTotal);
			break;
		default: assert(0); break;
		}

		for (std::size_t Layer = 0; Layer < Texture.layers(); ++Layer)
			for (std::size_t Face = 0; Face < Texture.faces(); ++Face)
				for (std::size_t Level = 0; Level < Texture.levels(); ++Level)
				{
					GLsizei const LayerGL = static_cast<GLsizei>(Layer);
					glm::tvec3<GLsizei> Dimensions(Texture.dimensions(Level));
					if (gli::is_target_cube(Texture.target()))
						Target = static_cast<GLenum>(GL_TEXTURE_CUBE_MAP_POSITIVE_X + Face);
					switch (Texture.target())
					{
					case gli::TARGET_1D:
						if (gli::is_compressed(Texture.format()))
							glCompressedTexSubImage1D(
								Target, static_cast<GLint>(Level), 0, Dimensions.x,
								GLenum(Format.Internal), static_cast<GLsizei>(Texture.size(Level)),
								Texture.data(Layer, Face, Level));
						else
							glTexSubImage1D(
								Target, static_cast<GLint>(Level), 0, Dimensions.x, GLenum(Format.External), GLenum(Format.Type),
								Texture.data(Layer, Face, Level));
						break;
					case gli::TARGET_1D_ARRAY:
					case gli::TARGET_2D:
					case gli::TARGET_CUBE:
						if (gli::is_compressed(Texture.format()))
							glCompressedTexSubImage2D(
								Target, static_cast<GLint>(Level), 0, 0,
								Dimensions.x, Texture.target() == gli::TARGET_1D_ARRAY ? LayerGL : Dimensions.y,
								GLenum(Format.Internal), static_cast<GLsizei>(Texture.size(Level)),
								Texture.data(Layer, Face, Level));
						else
							glTexSubImage2D(
								Target, static_cast<GLint>(Level), 0, 0,
								Dimensions.x, Texture.target() == gli::TARGET_1D_ARRAY ? LayerGL : Dimensions.y,
								GLenum(Format.External), GLenum(Format.Type), Texture.data(Layer, Face, Level));
						break;
					case gli::TARGET_2D_ARRAY:
					case gli::TARGET_3D:
					case gli::TARGET_CUBE_ARRAY:
						if (gli::is_compressed(Texture.format()))
							glCompressedTexSubImage3D(
								Target, static_cast<GLint>(Level), 0, 0, 0,
								Dimensions.x, Dimensions.y, Texture.target() == gli::TARGET_3D ? Dimensions.z : LayerGL,
								GLenum(Format.Internal), static_cast<GLsizei>(Texture.size(Level)),
								Texture.data(Layer, Face, Level));
						else
							glTexSubImage3D(
								Target, static_cast<GLint>(Level), 0, 0, 0,
								Dimensions.x, Dimensions.y, Texture.target() == gli::TARGET_3D ? Dimensions.z : LayerGL,
								GLenum(Format.External), GLenum(Format.Type), Texture.data(Layer, Face, Level));
						break;
					default: assert(0); break;
					}
				}

		glBindTexture(Target, 0);

		_width = Dimensions.x;
		_height = Dimensions.y;

		_name = TextureName;
		_handle = glGetTextureHandleARB(_name);
		glMakeTextureHandleResidentARB(_handle);
	}

	void Texture::LoadUpSDLTexture(SDL_Surface* surface)
	{
		_width = surface->w;
		_height = surface->h;

		glCreateTextures(GL_TEXTURE_2D, 1, &_name);

		glTextureStorage2D(_name, 1, GL_RGBA8, _width, _height);
		glTextureSubImage2D(_name, 0, 0, 0, _width, _height, GL_RGBA, GL_UNSIGNED_INT_8_8_8_8, surface->pixels);

		glTextureParameteri(_name, GL_TEXTURE_MIN_FILTER, (GLint)GL_LINEAR);
		glTextureParameteri(_name, GL_TEXTURE_MAG_FILTER, (GLint)GL_LINEAR);

		_handle = glGetTextureHandleARB(_name);
		glMakeTextureHandleResidentARB(_handle);
	}

	Uint32 Texture::GetPixel32(SDL_Surface *pSurfacePtr, int pX, int pY)
	{
		//Convert the pixels to 32 bit
		Uint32 *pixels = (Uint32 *)pSurfacePtr->pixels;

		//Get the requested pixel
		return pixels[(pY * pSurfacePtr->w) + pX];
	}

	void Texture::PutPixel32(SDL_Surface *pSurfacePtr, int pX, int pY, Uint32 pPixel)
	{
		//Convert the pixels to 32 bit
		Uint32 *pixels = (Uint32 *)pSurfacePtr->pixels;

		//Set the pixel
		pixels[(pY * pSurfacePtr->w) + pX] = pPixel;
	}

	SDL_Surface* Texture::FlipHorizontally(SDL_Surface* pImagePtr)
	{
		// create a copy of the image
		SDL_Surface* flipped_imagePtr = SDL_CreateRGBSurface(SDL_SWSURFACE, pImagePtr->w, pImagePtr->h, pImagePtr->format->BitsPerPixel,
			pImagePtr->format->Rmask, pImagePtr->format->Gmask, pImagePtr->format->Bmask, pImagePtr->format->Amask);

		// loop through pixels
		for (int x = 0; x < pImagePtr->w; x++)
		{
			for (int y = 0; y < pImagePtr->h; y++)
			{
				// copy pixels, but reverse the x pixels!
				PutPixel32(flipped_imagePtr, x, y, GetPixel32(pImagePtr, x, pImagePtr->h - y - 1));
			}
		}

		// free original and assign flipped to it
		SDL_FreeSurface(pImagePtr);
		return flipped_imagePtr;
	}
}