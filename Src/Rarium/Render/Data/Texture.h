//License : Please see Rarium.LICENSE
#ifndef _RariumTexture_

#define _RariumTexture_

#include <Rarium/Core/Rarium.h>
#include <string>
#include <glbinding/gl/gl.h>
#include <SDL2/SDL.h>
#include <glm/glm.hpp>

using namespace glm;

using namespace gl;

namespace Rarium
{
	/**
	*	An OpenGL texture
	*/
	class Rarium_API Texture
	{
	public:
		/**
		*	Create a new texture from a file
		*	@param pPath Path to the texture relative to the assets directory
		*/
		Texture(std::string pPath);
		/**
		*	Create a texture from a string text
		*	@param pText Text to draw in the texture
		*	@param pFont Path to the ttf file for the font relative to the assets directory
		*	@param pFontSize Size of the text to draw
		*	@param pColor Color of the text to draw
		*/
		Texture(std::string pText, std::string pFont, unsigned int pFontSize, vec3 pColor);
		/**
		*	Destroy the texture
		*/
		~Texture();

		/// Get the path of the texture
		inline std::string GetPath() { return _path; }
		/// Get the OpenGL name of the texture
		inline GLuint GetName() { return _name; }
		/// Get the width of the texture
		inline GLuint GetWidth() { return _width; }
		/// Get the height of the texture
		inline GLuint GetHeight() { return _height; }
		/// Get the handle of the texture on GPU
		inline GLuint64 GetHandle() { return _handle; }

	protected:
		/**
		*	Create the texture from a dds file
		*	@param pFilename Path to the dds file relative to the assets directory
		*/
		void CreateTexture(const char* pFilename);
		/**
		*	Create the texture from a SDL_Surface
		*	@param pSurfacePtr The SDL Surface
		*/
		void LoadUpSDLTexture(SDL_Surface* pSurfacePtr);

		/// Flip an SDL surface horizontally
		SDL_Surface* FlipHorizontally(SDL_Surface* pImagePtr);
		/// Get the the value of a pixel in an SDL_Surface
		Uint32 GetPixel32(SDL_Surface* pSurfacePtr, int pX, int pY);
		/// Set the value of a pixel in an SDL_Surface
		void PutPixel32(SDL_Surface* pSurfacePtr, int pX, int pY, Uint32 pPixel);

		std::string _path;///< Path to the texture
		GLuint _name;///< OpenGL name of the texture
		GLuint64 _handle;///< GPU handle of the texture
		GLuint _width;///< Width of the texture
		GLuint _height;///< Height of the texture
	};
}

#endif