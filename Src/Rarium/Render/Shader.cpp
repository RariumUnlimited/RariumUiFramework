﻿//License : Please see Rarium.LICENSE
#include <glm/glm.hpp>
#include <Rarium/Render/Shader.h>
#include <Rarium/Render/Renderer.h>
#include <Rarium/Render/GLUtil.h>
#include <Rarium/Core/Exception.h>
#include <sstream>
#include <map>

using namespace gl;
using namespace glm;

namespace Rarium
{
	Shader::Shader(std::string pName, std::string pSrc)
	{
		_name = pName;

		if (_name.find(".vs") != std::string::npos)
			_type = GL_VERTEX_SHADER;
		else if (_name.find(".fs") != std::string::npos)
			_type = GL_FRAGMENT_SHADER;
		else if (_name.find(".gs") != std::string::npos)
			_type = GL_GEOMETRY_SHADER;
		else if (_name.find(".cs") != std::string::npos)
			_type = GL_COMPUTE_SHADER;
		else if (_name.find(".tcs") != std::string::npos)
			_type = GL_TESS_CONTROL_SHADER;
		else if (_name.find(".tes") != std::string::npos)
			_type = GL_TESS_EVALUATION_SHADER;
		else
			throw Exception("No shader type found");

		GLuint tempShader = glCreateShader(_type);

		const char* tempSrcAR = pSrc.c_str();

		glShaderSource(tempShader, 1, &tempSrcAR, nullptr);
		glCompileShader(tempShader);

		GLint compileStatus = 0;
		GLint logSize = 0;
		GLsizei length = 0;
		GLchar* logAR;

		glGetShaderiv(tempShader, GL_COMPILE_STATUS, &compileStatus);

		if (compileStatus != (GLint)GL_TRUE)
		{
			glGetShaderiv(tempShader, GL_SHADER_SOURCE_LENGTH, &logSize);
			logAR = new GLchar[logSize];
			glGetShaderSource(tempShader, logSize, &length, logAR);
			Log("Shader " + _name + " source : ");
			Log(ToString(logAR));
			delete[] logAR;

			glGetShaderiv(tempShader, GL_INFO_LOG_LENGTH, &logSize);
			logAR = new GLchar[logSize];
			glGetShaderInfoLog(tempShader, logSize, &length, logAR);
			Log("Shader " + _name + " compile log : ");
			Log(ToString(logAR));
			delete[] logAR;

			throw Exception("Shader " + _name + " did not compile , see log above");
		}

		_shader = tempShader;

		CheckGLError();
	}

	Shader::~Shader()
	{
		glDeleteShader(_shader);
	}
}