//License : Please see Rarium.LICENSE
#ifndef _RariumProperty_

#define _RariumProperty_

#include <Rarium/Core/Rarium.h>
#include <string>
#include <sstream>

namespace Rarium
{
	/**
	*	Represent a user property. Has a category and a name associated to a value
	*	Type T must convertible to and from a string
	*/
	template<typename T>
	class Property
	{
	public:
		/**
		*	Build the property
		*	@param pCategory Category of the property
		*	@param pName Name of the property
		*	@param pValue Value of the property
		*/
		Property(std::string pCategory, std::string pName, T pValue) { _category = pCategory;  _name = pName; _value = pValue; }

		/// Get the category of the property
		inline std::string GetCategory() { return _category; }
		/// Get the name of the property
		inline std::string GetName() { return _name; }
		/// Get the value of the property
		inline T GetValue() { return _value; }

		/// Get the value of the property
		operator T() { return _value; }

		/// Set a new value for the property
		inline void SetValue(T pValue) { _value = pValue; }

	protected:
		std::string _category;///< Category of the property
		std::string _name;///< Name of the property
		T _value;///< Value of the property
	};
}

#endif