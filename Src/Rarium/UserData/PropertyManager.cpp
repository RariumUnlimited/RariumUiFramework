//License : Please see Rarium.LICENSE
#include <Rarium/UserData/PropertyManager.h>
#include <Rarium/Core/Exception.h>
#include <Rarium/Core/Log.h>

namespace Rarium
{
	PropertyManager::PropertyManager()
	{
		if (sqlite3_open_v2("UserData.dat", &_db, SQLITE_OPEN_READWRITE | SQLITE_OPEN_CREATE, nullptr) != SQLITE_OK)
			throw Exception("Unable to open/create user data file : UserData.dat");

		int result;

		sqlite3_stmt* createTableStmt;
		std::string createTableSql = "CREATE TABLE Properties(Category varchar(255), Name varchar(255), Value varchar(2048), PRIMARY KEY (Category,Name));";
		if (result = sqlite3_prepare_v2(_db, createTableSql.c_str(), createTableSql.size(), &createTableStmt, nullptr) != SQLITE_OK)
		{
			Log("Operation on user data file failure : Create Table Stmt Prepare : "+ ToString(result) +" : " + ToString(sqlite3_errmsg(_db)));
		}
		if (result = sqlite3_step(createTableStmt) != SQLITE_DONE)
		{
			Log("Operation on user data file failure : Create Table Stmt Step : " + ToString(result) + " : " + ToString(sqlite3_errmsg(_db)));
		}

		sqlite3_finalize(createTableStmt);

		sqlite3_stmt* selectStmt;
		std::string selectSql = "SELECT Category,Name,Value FROM Properties;";
		if (result = sqlite3_prepare_v2(_db, selectSql.c_str(), selectSql.size(), &selectStmt, nullptr) != SQLITE_OK)
		{
			Log("Operation on user data file failure : Select Stmt Prepare : " + ToString(result) + " : " + ToString(sqlite3_errmsg(_db)));
		}

		while (result = sqlite3_step(selectStmt) == SQLITE_ROW)
		{
			std::string category = std::string(reinterpret_cast<const char*>(sqlite3_column_text(selectStmt, 0)));
			std::string name = std::string(reinterpret_cast<const char*>(sqlite3_column_text(selectStmt, 1)));
			std::string value = std::string(reinterpret_cast<const char*>(sqlite3_column_text(selectStmt, 2)));
			_properties[category][name] = value;
		}

		sqlite3_finalize(selectStmt);

		
	}

	PropertyManager::~PropertyManager()
	{
		sqlite3_close(_db);
	}

	bool PropertyManager::CategoryExists(std::string pCategory)
	{
		return _properties.find(pCategory) != _properties.cend();
	}

	bool PropertyManager::PropertyExists(std::string pCategory, std::string pName)
	{
		if (CategoryExists(pCategory))
			return _properties[pCategory].find(pName) != _properties[pCategory].cend();
		else
			return false;
	}

	void PropertyManager::UpdatePropertyInDb(std::string pCategory, std::string pName, bool pNeedInsert)
	{
		int result;

		sqlite3_stmt* updateStmt;
		std::string updateSql;
		if (pNeedInsert)
		{
			updateSql = "INSERT INTO Properties(Category,Name,Value) VALUES ('" + pCategory + "','" + pName + "','" + _properties[pCategory][pName] + "');";
		}
		else
		{
			updateSql = "UPDATE Properties SET Value = '" + _properties[pCategory][pName] + "' WHERE Category = '" + pCategory + "' AND Name = '" + pName + "';";
		}
		
		if (result = sqlite3_prepare_v2(_db, updateSql.c_str(), updateSql.size(), &updateStmt, nullptr) != SQLITE_OK)
		{
			Log("Operation on user data file failure : Update Stmt Prepare : " + ToString(result) + " : " + ToString(sqlite3_errmsg(_db)));
		}
		if (result = sqlite3_step(updateStmt) != SQLITE_DONE)
		{
			Log("Operation on user data file failure : Update Stmt Step : " + ToString(result) + " : " + ToString(sqlite3_errmsg(_db)));
		}

		sqlite3_finalize(updateStmt);
	}

	PropertyManager PropertyManager::_instance;
}