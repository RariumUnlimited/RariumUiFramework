//License : Please see Rarium.LICENSE
#ifndef _RariumPropertyManager_

#define _RariumPropertyManager_

#include <Rarium/Core/Rarium.h>
#include <map>
#include <string>
#include <sstream>

#include <Rarium/UserData/Property.h>

#include <sqlite/sqlite3.h>

namespace Rarium
{
	/**
	*	Singleton used to manage user property.
	*	Properties are stored in an sqlite database.
	*/
	class Rarium_API PropertyManager
	{
	public:
		/// Check if a category exists
		bool CategoryExists(std::string pCategory);
		/// Check if a property exists in a given category
		bool PropertyExists(std::string pCategory, std::string pName);

		/// Get the property pName in the given category. Return default property if it is not found
		template<class T>
		Property<T> GetProperty(std::string pCategory, std::string pName)
		{
			if (PropertyExists(pCategory, pName))
				return Property<T>(pCategory, pName, ToObject<T>(_properties[pCategory][pName]));
			else
				return Property<T>(pCategory, pName, T());
		}

		/// Update the property in the database
		template<class T>
		void UpdateProperty(Property<T> pProperty)
		{
			bool needInsert = !PropertyExists(pProperty.GetCategory(), pProperty.GetName());
			_properties[pProperty.GetCategory()][pProperty.GetName()] = ToString(pProperty.GetValue());
			UpdatePropertyInDb(pProperty.GetCategory(), pProperty.GetName(), needInsert);
		}

		/// Get a reference to the unique instance of the manager
		static PropertyManager& GetRef() { return _instance; }

	private:

		std::map<std::string, std::map<std::string, std::string>> _properties;///< Data Category -> (Data Name -> Data Value)
		sqlite3* _db;///< Pointer to the sqlite database.

		/// Build a new manager
		PropertyManager();
		/// Destroy the manager : close the sqlite database
		~PropertyManager();
		/// Update a property in the sqlite database.
		void UpdatePropertyInDb(std::string pCategory, std::string pName, bool pNeedInsert);

		static PropertyManager _instance;/// unique instance of the manager.
	};
}

#endif