//License : Please see Rarium.LICENSE
#ifndef _RariumUiDescriptor_

#define _RariumUiDescriptor_

#include <Rarium/Core/Rarium.h>
#include <Rarium/Ui/Ui.h>
#include <Rarium/Core/Condition.h>
#include <string>

namespace Rarium
{
	/**
	*	Contain informations about a ui that are needed before create a ui. Can also be used to create the associated ui.
	*/
	class Rarium_API UiDescriptor
	{
	public:
		/**
		*	Create a new descriptor
		*	@param pName Name of the ui
		*	@param pPreviewTexturePath Path to a texture that preview the look of the ui, used for the menu
		*	@param pConditionPtr An optionnal condition that need to be valid so that we can display the ui
		*/
		UiDescriptor(std::string pName, std::string pPreviewTexturePath, Condition* pConditionPtr = new Condition());
		/**
		*	Destroy the ui
		*/
		~UiDescriptor();

		/**
		*	Return a new Ui corresponding to the descriptor
		*/
		virtual Ui* GetUi() = 0;

		/// Get if the ui can be display and is accessible in the menu
		inline bool IsAccessible() { return _conditionPtr->Check(); }
		/// Get if we display the menu overlay when this ui is displayed
		inline bool DisplayMenu() { return _displayMenu; }
		/// Get the name of the ui
		inline std::string GetName() { return _name; }
		/// Get the path to the ui preview texture
		inline std::string GetPreviewTexturePath() { return _previewTexturePath; }

	protected:
		std::string _name;///< Name of the ui
		std::string _previewTexturePath;///< Path to the ui preview texture
		Condition* _conditionPtr;///< Condition that need to be valid so that we can display the ui
		bool _displayMenu;///< True if we display the menu overlay when this ui is displayed
	};
}

#endif