//License : Please see Rarium.LICENSE
#include <Rarium/Ui/UiElements/MultiTextElement.h>

#include <Rarium/Core/Math.h>

namespace Rarium
{
	MultiTextElement::MultiTextElement(Ui* pUi, std::string pId, vec2 pPosition, std::vector<std::string> pText, std::string pFont, unsigned int pFontSize, vec3 pColor, UiElement* pParent) : UiElement(pUi,pId,pPosition,pParent)
	{

		for (std::string &s : pText)
		{
			TextData data;
			data.Text = s;
			data.Font = pFont;
			data.FontSize = pFontSize;
			data.Color = pColor;
			_text.push_back(data);
		}

		for (unsigned int i = 0; i < _text.size(); i++)
			_resources.push_back(UiResource(pId + std::to_string(i), UiResourceType::Text, &_text.data()[i]));

		_drawIndex = 0;
	}

	void MultiTextElement::UiElementLoaded(RenderFunction* pFunctionPtr)
	{
		UiElement::UiElementLoaded(pFunctionPtr);
		_size = pFunctionPtr->GetResourceSize(_id + std::to_string(_drawIndex), UiResourceType::Text);
	}

	void MultiTextElement::Draw()
	{
		_functionPtr->DrawText(_id + std::to_string(_drawIndex), _position);
	}

	void MultiTextElement::UpdateDrawIndex(unsigned int pIndex)
	{
		_drawIndex = pIndex;
		_drawIndex = Clamp(_drawIndex, (unsigned int)0, _text.size() - 1);
		_size = _functionPtr->GetResourceSize(_id + std::to_string(_drawIndex), UiResourceType::Text);
	}
}