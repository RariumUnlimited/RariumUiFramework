//License : Please see Rarium.LICENSE
#include <Rarium/Ui/UiElements/MenuElement.h>
#include <Rarium/Core/Log.h>

namespace Rarium
{
	MenuElement::MenuElement(Ui* pUi, std::string pId, std::vector<UiDescriptor*> pDesciptors) : UiElement(pUi,pId,vec2(0.0,670.0))
	{
		_firstDescriptorDisplayed = 0;
		_isRetracted = true;

		_menuBackground = new ImageElement(pUi,"Rarium/MenuBackground.dds", vec2(0.0, 50.0), this);
		_children.push_back(_menuBackground);

		_menuButton = new ButtonElement(pUi, "MenuDisplayButton", vec2(50.0, 0.0), "Rarium/MenuDisplayButton.dds", "Rarium/MenuDisplayButtonActive.dds", "Rarium/MenuDisplayButtonHover.dds","Menu", "Rarium/redline.ttf", 25, vec3(1.0, 1.0, 1.0),this);
		_menuButton->Click += new Event<void, MouseEventArg>::T<MenuElement>(this, &MenuElement::MenuDisplayButtonClick);
		_children.push_back(_menuButton);

		_menuButtonPrevious = new ButtonElement(pUi, "MenuButtonPrevious", vec2(20.0, 60.0), "Rarium/MenuButton.dds", "Rarium/MenuButtonActive.dds", "Rarium/MenuButtonHover.dds", "Previous", "Rarium/redline.ttf", 20, vec3(1.0, 1.0, 1.0), this);
		_menuButtonPrevious->Click += new Event<void, MouseEventArg>::T<MenuElement>(this, &MenuElement::MenuPreviousButtonClick);
		_children.push_back(_menuButtonPrevious);
		
		_menuButtonNext = new ButtonElement(pUi, "MenuButtonNext", vec2(280.0, 60.0), "Rarium/MenuButton.dds", "Rarium/MenuButtonActive.dds", "Rarium/MenuButtonHover.dds", "Next", "Rarium/redline.ttf", 20, vec3(1.0, 1.0, 1.0), this);
		_menuButtonNext->Click += new Event<void, MouseEventArg>::T<MenuElement>(this, &MenuElement::MenuNextButtonClick);
		_children.push_back(_menuButtonNext);

		for (UiDescriptor* descPtr : pDesciptors)
		{
			_allDescriptors.push_back(std::make_pair(descPtr, new ImageElement(pUi,descPtr->GetPreviewTexturePath(), vec2(0.0, 0.0), this)));
			_children.push_back(_allDescriptors.back().second);
			_allDescriptors.back().second->SetEnabled(false);
			if(descPtr->IsAccessible())
				_displayableDescriptor.push_back(_allDescriptors.back());
		}

		ResetMenuUiButton(0);
	}

	void MenuElement::ResetMenuUiButton(int pOffset)
	{
		float startY = 512;

		for (unsigned int i = _firstDescriptorDisplayed; i < _firstDescriptorDisplayed + 3 && i < _displayableDescriptor.size(); i++)
		{
			_displayableDescriptor[i].second->SetEnabled(false);
			_displayableDescriptor[i].second->Click.Clear();
		}

		_firstDescriptorDisplayed += pOffset;

		for (unsigned int i = _firstDescriptorDisplayed; i < _firstDescriptorDisplayed + 3 && i < _displayableDescriptor.size(); i++)
		{
			_displayableDescriptor[i].second->MoveToPosition(vec2(50, startY));
			_displayableDescriptor[i].second->SetEnabled(true);

			if (startY == 512)
				_displayableDescriptor[i].second->Click += new Event<void, MouseEventArg>::T<MenuElement>(this, &MenuElement::MenuUi1ButtonClick);
			else if (startY == 319)
				_displayableDescriptor[i].second->Click += new Event<void, MouseEventArg>::T<MenuElement>(this, &MenuElement::MenuUi2ButtonClick);
			else
				_displayableDescriptor[i].second->Click += new Event<void, MouseEventArg>::T<MenuElement>(this, &MenuElement::MenuUi3ButtonClick);

			startY -= 193;
		}
	}

	void MenuElement::MenuDisplayButtonClick(MouseEventArg e)
	{
		_isRetracted = !_isRetracted;
		if (_isRetracted)
			MoveToPosition(vec2(0.0, 670.0));
		else
		{
			_displayableDescriptor.clear();
			for (std::pair<UiDescriptor*, ImageElement*> pair : _allDescriptors)
			{
				if (pair.first->IsAccessible())
					_displayableDescriptor.push_back(pair);
			}

			ResetMenuUiButton(0);

			MoveToPosition(vec2(0.0, 20.0));
		}
	}

	void MenuElement::MenuPreviousButtonClick(MouseEventArg e)
	{
		if (_firstDescriptorDisplayed > 2)
		{
			ResetMenuUiButton(-3);
		}
	}

	void MenuElement::MenuNextButtonClick(MouseEventArg e)
	{
		int temp = (int(_displayableDescriptor.size()) - 3);
		int tempBis = _firstDescriptorDisplayed;
		if (tempBis < temp)
		{
			ResetMenuUiButton(3);
		}
	}

	void MenuElement::MenuUi1ButtonClick(MouseEventArg e)
	{
		Log("Ui Button 1 : " + _displayableDescriptor[_firstDescriptorDisplayed].first->GetName());
		_ui->GetUiFunctionPtr()->RequestUiChange(_displayableDescriptor[_firstDescriptorDisplayed].first->GetName());
	}

	void MenuElement::MenuUi2ButtonClick(MouseEventArg e)
	{
		Log("Ui Button 2 : " + _displayableDescriptor[_firstDescriptorDisplayed + 1].first->GetName());
		_ui->GetUiFunctionPtr()->RequestUiChange(_displayableDescriptor[_firstDescriptorDisplayed+1].first->GetName());
	}

	void MenuElement::MenuUi3ButtonClick(MouseEventArg e)
	{
		Log("Ui Button 3 : " + _displayableDescriptor[_firstDescriptorDisplayed + 2].first->GetName());
		_ui->GetUiFunctionPtr()->RequestUiChange(_displayableDescriptor[_firstDescriptorDisplayed+2].first->GetName());
	}

	void MenuElement::Draw()
	{
		if (_isRetracted)
		{
			_menuButton->Draw();
		}
		else
		{
			_menuBackground->Draw();
			_menuButton->Draw();
			_menuButtonPrevious->Draw();
			_menuButtonNext->Draw();
			for (unsigned int i = _firstDescriptorDisplayed; i < _firstDescriptorDisplayed + 3 && i < _displayableDescriptor.size(); i++)
			{
				_displayableDescriptor[i].second->Draw();
			}
		}
	}
}