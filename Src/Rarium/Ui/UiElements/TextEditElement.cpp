//License : Please see Rarium.LICENSE
#include <Rarium/Ui/UiElements/TextEditElement.h>

namespace Rarium
{
	TextEditElement::TextEditElement(Ui* pUi, std::string pId, vec2 pPosition, std::string pSprite, std::string pSpriteFocus, std::string pFont, unsigned int pFontSize, vec3 pColor, UiElement* pParent) : TextElement(pUi,pId, pPosition, "", pFont, pFontSize,true, pColor, pParent)
	{
		_requireUpdate = false;
		_sprite.Path = pSprite;
		_spriteFocus.Path = pSpriteFocus;

		_resources.push_back(UiResource(_sprite.Path, UiResourceType::Texture,&_sprite));
		_resources.push_back(UiResource(_spriteFocus.Path, UiResourceType::Texture,&_spriteFocus));
	}

	void TextEditElement::UiElementLoaded(RenderFunction* pFunctionPtr)
	{
		TextElement::UiElementLoaded(pFunctionPtr);

		_size = pFunctionPtr->GetResourceSize(_sprite.Path, UiResourceType::Texture);
	}

	void TextEditElement::Draw()
	{
		_requireUpdate = false;
		if(_state.IsFocus)
			_functionPtr->DrawImage(_spriteFocus.Path, _position);
		else
			_functionPtr->DrawImage(_sprite.Path, _position);


		vec2 textSize = _functionPtr->GetResourceSize(_id, UiResourceType::Text);
		vec2 spriteSize = _functionPtr->GetResourceSize(_sprite.Path, UiResourceType::Texture);

		vec2 posT(_position.x + (spriteSize.x / 2) - (textSize.x / 2), _position.y + (spriteSize.y / 2) - (textSize.y / 2));

		_functionPtr->DrawText(_id, posT);
	}

	void TextEditElement::Update(float pDelta, InputState* pInputState, InputState* pLastInputState)
	{
		TextElement::Update(pDelta,pInputState, pLastInputState);

		if (_state.IsFocus && !_state.WasFocus)
		{
			TextElement::UpdateText(_text.Text + "_");
		}

		if (_state.IsFocus)
		{
			std::string textTemp = pInputState->UpdateText(_realText);
			if (textTemp != _realText)
			{
				_realText = textTemp;
				TextElement::UpdateText(textTemp + "_");
			}
		}
		
		if (_state.WasFocus && !_state.IsFocus)
		{
			TextElement::UpdateText(_realText);
		}
	}
}