//License : Please see Rarium.LICENSE
#ifndef _RariumTextEditElement_

#define _RariumTextEditElement_

#include <Rarium/Core/Rarium.h>
#include <Rarium/Ui/UiElements/TextElement.h>

namespace Rarium
{
	/**
	*	A text edit element to get some user input text
	*/
	class Rarium_API TextEditElement : public TextElement
	{
	public:
		/**
		*	Create a new text edit element
		*	@param pUi The ui possessing this element
		*	@param pPosition Position on screen of the element
		*	@param pSprite Sprite to display by default for the element
		*	@param pSpriteFocus Sprite to display when the element is focused
		*	@param pFont Name of the font to use to draw the text
		*	@param pFontSize Size of the text to draw
		*	@param pColor The color of the text
		*	@param pParent The optionnal father of this element
		*/
		TextEditElement(Ui* pUi, std::string pId, vec2 pPosition, std::string pSprite, std::string pSpriteFocus, std::string pFont, unsigned int pFontSize, vec3 pColor = vec3(0.0, 0.0, 0.0), UiElement* pParent = nullptr);
		/// Function that is called by the renderer when the resources of the element have been loaded on gpu
		virtual void UiElementLoaded(RenderFunction* functionPtr) override;
		/// Draw the text edit element on screen
		virtual void Draw() override;
		/// Update the text depending on user input
		virtual void Update(float pDelta, InputState* pInputState, InputState* pLastInputState) override;

		/// Get the text input by the user (_text without '_')
		inline std::string GetRealText() { return _realText; }

	protected:
		TextureData _sprite;
		TextureData _spriteFocus;
		std::string _realText;/// The text input by the user(_text without '_')
	};
}

#endif