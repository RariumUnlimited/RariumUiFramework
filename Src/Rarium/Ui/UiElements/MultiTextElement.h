//License : Please see Rarium.LICENSE
#ifndef _RariumMultiTextElement_

#define _RariumMultiTextElement_

#include <Rarium/Core/Rarium.h>
#include <Rarium/Ui/UiElement.h>

namespace Rarium
{
	/**
	*	A text element that can display several predefined text
	*/
	class Rarium_API MultiTextElement : public UiElement
	{
	public:
		/**
		*	Create a new multi text element
		*	@param pUi The ui possessing this element
		*	@param pPosition Position on screen of the element
		*	@param pText The list of text that can be displayed
		*	@param pFont Name of the font to use to draw the text
		*	@param pFontSize Size of the text to draw
		*	@param pIsDynamic If the text is dynamic
		*	@param pColor The color of the text
		*	@param pParent The optionnal father of this element
		*/
		MultiTextElement(Ui* pUi, std::string pId, vec2 pPosition, std::vector<std::string> pText, std::string pFont, unsigned int pFontSize, vec3 pColor = vec3(0.0, 0.0, 0.0), UiElement* pParent = nullptr);
		/// Function that is called by the renderer when the resources of the element have been loaded on gpu
		virtual void UiElementLoaded(RenderFunction* pFunctionPtr) override;
		/// Draw the selected text on screen
		virtual void Draw() override;

		/// Get the list of text that can be display
		inline std::vector<TextData> GetText() { return _text; }
		/// Get the font used by this element
		inline std::string GetFont() { return _text.front().Font; }
		/// Get the size of the text
		inline unsigned int GetFontSize() { return _text.front().FontSize; }
		/// Get the color of the element
		inline vec3 GetColor() { return _text.front().Color; }
		/// Get the text that is currently displayed
		inline unsigned int GetDrawIndex() { return _drawIndex; }
		
		/// Update the text that is currently displayed
		void UpdateDrawIndex(unsigned int pIndex);

	protected:
		std::vector<TextData> _text;/// List of text that can be display
		unsigned int _drawIndex;/// Index of the text currently displayed
	};
}

#endif