//License : Please see Rarium.LICENSE
#include <Rarium/Ui/UiElements/ImageElement.h>

namespace Rarium
{
	ImageElement::ImageElement(Ui* pUi, std::string pId, vec2 pPosition, UiElement* pParent) : UiElement(pUi,pId,pPosition,pParent)
	{
		_sprite.Path = pId;
		_resources.push_back(UiResource(pId, UiResourceType::Texture,&_sprite));
	}

	void ImageElement::UiElementLoaded(RenderFunction* pFunctionPtr)
	{
		UiElement::UiElementLoaded(pFunctionPtr);
		_size = pFunctionPtr->GetResourceSize(_id, UiResourceType::Texture);
	}

	void ImageElement::Draw()
	{
		_functionPtr->DrawImage(_id, _position);
	}
}