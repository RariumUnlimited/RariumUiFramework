//License : Please see Rarium.LICENSE
#ifndef _RariumMenuElement_

#define _RariumMenuElement_

#include <Rarium/Core/Rarium.h>
#include <Rarium/Ui/UiDescriptor.h>
#include <Rarium/Ui/UiElement.h>
#include <Rarium/Ui/UiElements/ButtonElement.h>
#include <Rarium/Ui/UiElements/ImageElement.h>
#include <Rarium/Ui/UiElements/TextElement.h>
#include <string>

namespace Rarium
{
	/**
	*	A menu that can be displayed to switch between ui
	*	There is a button that is used to expend or retract the menu
	*	3 ui preview a available per page of menu, two button can be used to switch page
	*/
	class Rarium_API MenuElement : public UiElement
	{
	public:
		/**
		*	Create a new menu
		*	@param pUi The ui possessing the menu
		*	@param pId Id of the menu
		*	@param pDescriptors The list of all ui descriptor loaded
		*/
		MenuElement(Ui* pUi, std::string pId, std::vector<UiDescriptor*> pDesciptors);
		/// Draw the menu on screen
		virtual void Draw() override;

	protected:
		/// Function called when the display/hide button is clicked, will display or hide depending on current status
		void MenuDisplayButtonClick(MouseEventArg e);
		/// Display the previous page of the menu
		void MenuPreviousButtonClick(MouseEventArg e);
		/// Display the next page of the menu
		void MenuNextButtonClick(MouseEventArg e);

		/// Display the first ui displayed in the menu
		void MenuUi1ButtonClick(MouseEventArg e);
		/// Display the second ui displayed in the menu
		void MenuUi2ButtonClick(MouseEventArg e);
		/// Display the third ui displayed in the menu
		void MenuUi3ButtonClick(MouseEventArg e);

		void ResetMenuUiButton(int pOffset);

		std::vector<std::pair<UiDescriptor*, ImageElement*>> _allDescriptors;///< List of all ui descriptors loaded
		std::vector<std::pair<UiDescriptor*, ImageElement*>> _displayableDescriptor;///< List of all descriptors for ui that can be displayed
		unsigned int _firstDescriptorDisplayed;///< The index of the first preview displyed in the menu
		bool _isRetracted;///< If the menu is shown or hidden

		ButtonElement* _menuButton;///< The button to display/hide the menu
		ButtonElement* _menuButtonPrevious;///< The button to go on the previous page of the menu
		ButtonElement* _menuButtonNext;///< The button to go on next page of the menu
		ImageElement* _menuBackground;///< The image of the background

	};
}

#endif