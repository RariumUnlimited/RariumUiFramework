//License : Please see Rarium.LICENSE
#include <Rarium/Ui/UiElements/ShaderToyElement.h>

namespace Rarium
{
	ShaderToyElement::ShaderToyElement(Ui* pUi, std::string pId, UiElement* pParent, std::string pTex1, std::string pTex2, std::string pTex3, std::string pTex4) : UiElement(pUi,pId,vec2(0.0,0.0),pParent)
	{
		_tex1.Path = pTex1;
		_tex2.Path = pTex2;
		_tex3.Path = pTex3;
		_tex4.Path = pTex4;
		_shader.SrcPath = pId;
		if (pTex1 != "")
			_resources.push_back(UiResource(pTex1, UiResourceType::Texture,&_tex1));
		if (pTex2 != "")
			_resources.push_back(UiResource(pTex2, UiResourceType::Texture, &_tex2));
		if (pTex3 != "")
			_resources.push_back(UiResource(pTex3, UiResourceType::Texture, &_tex3));
		if (pTex4 != "")
			_resources.push_back(UiResource(pTex4, UiResourceType::Texture, &_tex4));
		_resources.push_back(UiResource(pId, UiResourceType::UserShader,&_shader));
	}

	ShaderToyElement::ShaderToyElement(Ui* pUi, std::string pId, vec2 pPosition, vec2 pSize, UiElement* pParent, std::string pTex1, std::string pTex2, std::string pTex3, std::string pTex4) : ShaderToyElement(pUi,pId,pParent, pTex1,pTex2,pTex3,pTex4)
	{
		_position = pPosition;
		if (pParent != nullptr)
			_position += pParent->GetPosition();
		_size = pSize;
	}

	void ShaderToyElement::UiElementLoaded(RenderFunction* pFunctionPtr)
	{
		UiElement::UiElementLoaded(pFunctionPtr);
		if(_size.x == 0)
			_size = pFunctionPtr->GetViewportSize();
	}

	void ShaderToyElement::Draw()
	{
		_functionPtr->ShaderToy(_id, _position, _size, _tex1.Path, _tex2.Path, _tex3.Path, _tex4.Path);
	}
}