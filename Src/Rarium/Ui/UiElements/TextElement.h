//License : Please see Rarium.LICENSE
#ifndef _RariumTextElement_

#define _RariumTextElement_

#include <Rarium/Core/Rarium.h>
#include <Rarium/Ui/UiElement.h>

namespace Rarium
{
	/**
	*	A text displayed on screen
	*/
	class Rarium_API TextElement : public UiElement
	{
	public:
		/**
		*	Create a new text element
		*	@param pUi The ui possessing this element
		*	@param pId Id of the element
		*	@param pPosition Position on screen of the element
		*	@param pText The text to draw
		*	@param pFont Name of the font to use to draw the text
		*	@param pFontSize Size of the text to draw
		*	@param pIsDynamic If the text is dynamic
		*	@param pColor The color of the text
		*	@param pParent The optionnal father of this element
		*/
		TextElement(Ui* pUi, std::string pId, vec2 pPosition, std::string pText, std::string pFont, unsigned int pFontSize, bool pIsDynamic = false, vec3 pColor = vec3(0.0, 0.0, 0.0), UiElement* pParent = nullptr);
		/**
		*	Create a new text element
		*	@param pUi The ui possessing this element
		*	@param pId Id of the element
		*	@param pPosition Position on screen of the element
		*	@param pText The text to draw
		*	@param pFont Name of the font to use to draw the text
		*	@param pFontSize Size of the text to draw
		*	@param pIsDynamic If the text is dynamic
		*	@param pColor The color of the text
		*	@param pParent The optionnal father of this element
		*/
		TextElement(Ui* pUi, std::string pId, vec2 pPosition, std::string pText, std::string pFont, unsigned int pFontSize, vec3 pColor = vec3(0.0, 0.0, 0.0), bool pIsDynamic = false, UiElement* pParent = nullptr);
		
		/// Function that is called by the renderer when the resources of the element have been loaded on gpu
		virtual void UiElementLoaded(RenderFunction* pFunctionPtr) override;
		/// Draw the text on screen
		virtual void Draw() override;

		/// Update the text displayed by this element
		void UpdateText(std::string text);

		/// Get the text display by this element
		inline std::string GetText() { return _text.Text; }
		/// Get the font used by this element
		inline std::string GetFont() { return _text.Font; }
		/// Get the size of the text
		inline unsigned int GetFontSize() { return _text.FontSize; }
		/// Get the color of the element
		inline vec3 GetColor() { return _text.Color; }

	protected:
		TextData _text;///< Text displayed by this element
	};
}

#endif