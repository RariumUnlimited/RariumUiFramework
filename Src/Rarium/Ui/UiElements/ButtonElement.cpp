//License : Please see Rarium.LICENSE
#include <Rarium/Ui/UiElements/ButtonElement.h>

namespace Rarium
{
	ButtonElement::ButtonElement(Ui* pUi, std::string pId, vec2 pPosition, std::string pSprite, std::string pSpriteActive, std::string pSpriteHover, std::string pText, std::string pFont, unsigned int pFontSize, vec3 pColor, UiElement* pParent) : TextElement(pUi,pId,pPosition,pText,pFont,pFontSize,pColor,false,pParent)
	{
		_sprite.Path = pSprite;
		_spriteActive.Path = pSpriteActive;
		_spriteHover.Path = pSpriteHover;
		_resources.push_back(UiResource(pSprite, UiResourceType::Texture,&_sprite));
		_resources.push_back(UiResource(pSpriteActive, UiResourceType::Texture, &_spriteActive));
		_resources.push_back(UiResource(pSpriteHover, UiResourceType::Texture, &_spriteHover));
	}

	void ButtonElement::UiElementLoaded(RenderFunction* pFunctionPtr)
	{
		UiElement::UiElementLoaded(pFunctionPtr);
		_textSize = pFunctionPtr->GetResourceSize(_id, UiResourceType::Text);
		_size = pFunctionPtr->GetResourceSize(_sprite.Path, UiResourceType::Texture);
	}

	void ButtonElement::Draw()
	{
		if (_state.IsActive)
			_functionPtr->DrawImage(_spriteActive.Path, _position);
		else if (_state.IsHover)
			_functionPtr->DrawImage(_spriteHover.Path, _position);
		else
			_functionPtr->DrawImage(_sprite.Path, _position);

		vec2 textPosition(_position.x + (_size.x / 2) - (_textSize.x / 2), _position.y + (_size.y / 2) - (_textSize.y / 2));

		_functionPtr->DrawText(_id, textPosition);
	}
}