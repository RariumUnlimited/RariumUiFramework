//License : Please see Rarium.LICENSE
#include <Rarium/Ui/UiElements/TextElement.h>
#include <Rarium/Core/Log.h>

namespace Rarium
{
	TextElement::TextElement(Ui* pUi, std::string pId, vec2 pPosition, std::string pText, std::string pFont, unsigned int pFontSize, bool pIsDynamic, vec3 pColor, UiElement* pParent) : UiElement(pUi,pId,pPosition, pParent,pIsDynamic)
	{
		_text.Text = pText;
		_text.Font = pFont;
		_text.FontSize = pFontSize;
		_text.Color = pColor;
		_resources.push_back(UiResource(pId, UiResourceType::Text, &_text,pIsDynamic));
	}

	TextElement::TextElement(Ui* pUi, std::string pId, vec2 pPosition, std::string pText, std::string pFont, unsigned int pFontSize, vec3 pColor, bool pIsDynamic, UiElement* pParent) : TextElement(pUi,pId,pPosition,pText,pFont,pFontSize,pIsDynamic,pColor,pParent)
	{

	}

	void TextElement::UiElementLoaded(RenderFunction* pFunctionPtr)
	{
		UiElement::UiElementLoaded(pFunctionPtr);
		_size = pFunctionPtr->GetResourceSize(_id, UiResourceType::Text);
	}

	void TextElement::Draw()
	{
		_functionPtr->DrawText(_id, _position);
	}

	void TextElement::UpdateText(std::string pText)
	{
		if (IsDynamic())
		{
			_text.Text = pText;
			_requireUpdate = true;
		}
		else
			Log("Trying to update a static text element : " + _id);
	}
}