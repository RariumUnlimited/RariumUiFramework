//License : Please see Rarium.LICENSE
#ifndef _RariumButtonElement_

#define _RariumButtonElement_

#include <Rarium/Core/Rarium.h>
#include <Rarium/Ui/UiElements/TextElement.h>

namespace Rarium
{
	/**
	*	A button that can be clicked
	*/
	class Rarium_API ButtonElement : public TextElement
	{
	public:
		/**
		* Create a new button
		*	@param pUi The ui possessing this element
		*	@param pId Id of the element
		*	@param pPosition Position on screen of the element
		*	@param pSprite Sprite to draw by default
		*	@param pSpriteActive Sprite to draw when the button is clicked
		*	@param pSpriteHover Sprite to draw when the mouse is over the button
		*	@param pText The text to draw on the button
		*	@param pFont Name of the font to use to draw the text
		*	@param pFontSize Size of the text to draw
		*	@param pColor The color of the text
		*	@param pParent The optionnal father of this element
		*/
		ButtonElement(Ui* pUi, std::string pId, vec2 pPosition, std::string pSprite, std::string pSpriteActive, std::string pSpriteHover, std::string pText, std::string pFont, unsigned int pFontSize, vec3 pColor = vec3(0.0, 0.0, 0.0), UiElement* pParent = nullptr);
		/// Function that is called by the renderer when the resources of the element have been loaded on gpu
		virtual void UiElementLoaded(RenderFunction* pFunctionPtr) override;
		/// Draw the button on screen
		virtual void Draw() override;

	protected:
		TextureData _sprite;///< Sprite to draw by default
		TextureData _spriteActive;///< Sprite to draw when the button is clicked
		TextureData _spriteHover;///< Sprite to draw when the mouse is over the button
		vec2 _textSize;///< The size of the text after being rendered

	};
}

#endif