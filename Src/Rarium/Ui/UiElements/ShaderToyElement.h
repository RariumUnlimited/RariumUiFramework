//License : Please see Rarium.LICENSE
#ifndef _RariumShaderToyElement_

#define _RariumShaderToyElement_

#include <Rarium/Core/Rarium.h>

#include <Rarium/Ui/UiElement.h>

namespace Rarium
{
	/**
	*	A shader toy widget (from the site : https://www.shadertoy.com/)
	*/
	class Rarium_API ShaderToyElement : public UiElement
	{
	public:
		/**
		*	Create a new shader toy widget full screen
		*	@param pUi The ui possessing this element
		*	@param pId Id of the element, the path to the fragment shader file
		*	@param pParent The optionnal father of this element
		*	@param pTex1 An optionnal texture used by the shader
		*	@param pTex2 An optionnal texture used by the shader
		*	@param pTex3 An optionnal texture used by the shader
		*	@param pTex4 An optionnal texture used by the shader
		*/
		ShaderToyElement(Ui* pUi, std::string pId, UiElement* pParent = nullptr, std::string pTex1 = "", std::string pTex2 = "", std::string pTex3 = "", std::string pTex4 = "");
		/**
		*	Create a new shader toy widget
		*	@param pUi The ui possessing this element
		*	@param pId Id of the element
		*	@param pPosition Position of the widget on screen
		*	@param pSize Size of the widget
		*	@param pParent The optionnal father of this element
		*	@param pTex1 An optionnal texture used by the shader
		*	@param pTex2 An optionnal texture used by the shader
		*	@param pTex3 An optionnal texture used by the shader
		*	@param pTex4 An optionnal texture used by the shader
		*/
		ShaderToyElement(Ui* pUi, std::string pId, vec2 pPosition, vec2 pSize, UiElement* pParent = nullptr, std::string pTex1 = "", std::string pTex2 = "", std::string pTex3 = "", std::string pTex4 = "");
		/// Function that is called by the renderer when the resources of the element have been loaded on gpu
		virtual void UiElementLoaded(RenderFunction* pFunctionPtr) override;
		/// Draw the shadertoy on screen
		virtual void Draw() override;


	protected:
		TextureData _tex1;///< An optionnal texture used by the shader
		TextureData _tex2;///< An optionnal texture used by the shader
		TextureData _tex3;///< An optionnal texture used by the shader
		TextureData _tex4;///< An optionnal texture used by the shader
		ShaderData _shader;///< The shader for the widget
	};
}

#endif