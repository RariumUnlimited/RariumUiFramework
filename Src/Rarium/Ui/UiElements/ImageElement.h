//License : Please see Rarium.LICENSE
#ifndef _RariumImageElement_

#define _RariumImageElement_

#include <Rarium/Core/Rarium.h>
#include <Rarium/Ui/UiElement.h>

namespace Rarium
{
	/**
	*	An image displayed on screen
	*/
	class Rarium_API ImageElement : public UiElement
	{
	public:
		/**
		*	Create a new image element
		*	@param pUi The ui possessing this element
		*	@param pId Path of the texture to use
		*	@param pPosition Position of the element on screen
		*	@param pParent Optionnal parent of the element
		*/
		ImageElement(Ui* pUi, std::string pId, vec2 pPosition, UiElement* pParent = nullptr);
		/// Function that is called by the renderer when the resources of the element have been loaded on gpu
		virtual void UiElementLoaded(RenderFunction* pFunctionPtr) override;
		/// Draw the image on screen
		virtual void Draw() override;

	protected:
		TextureData _sprite;/// The image to draw on screen
	};
}

#endif