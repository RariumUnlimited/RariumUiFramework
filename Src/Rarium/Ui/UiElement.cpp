//License : Please see Rarium.LICENSE
#include <Rarium/Ui/UiElement.h>
#include <Rarium/Core/Log.h>

namespace Rarium
{
	bool UiElement::_mouseIntercepted = false;

	UiElement::UiElement(Ui* pUi, std::string pId, vec2 pPosition, UiElement* pParent, bool pIsDynamic)
	{
		_ui = pUi;
		_id = pId;
		_isDynamic = pIsDynamic;
		_position = pPosition;
		_parent = pParent;
		_requireUpdate = false;
		_isEnabled = true;

		if (pParent != nullptr)
		{
			_position += pParent->GetPosition();
			pParent->Move += new Event<void, vec2>::T<UiElement>(this, &UiElement::MoveFromOffset);
		}
	}

	UiElement::~UiElement()
	{
		for (UiElement* childPtr : _children)
			delete childPtr;
	}

	void UiElement::MoveToPosition(vec2 pPosition)
	{
		if (_parent != nullptr)
			pPosition += _parent->GetPosition();

		vec2 offset = pPosition - _position;
		_position = pPosition;
		Move(offset);
	}

	void UiElement::MoveFromOffset(vec2 pOffset)
	{
		_position += pOffset;
		Move(pOffset);
	}

	void UiElement::Update(float pDelta, InputState* pInputState, InputState* pLastInputState)
	{
		if (_isEnabled)
		{
			for (int i = _children.size() - 1; i >= 0; i--)
				_children[i]->Update(pDelta,pInputState, pLastInputState);

			_state.WasHover = _state.IsHover;
			_state.WasFocus = _state.IsFocus;
			_state.WasClicked = _state.IsClicked;
			_state.WasActive = _state.IsActive;

			MouseEventArg eventArgs(pInputState->GetMousePosition());

			if (pInputState->GetMousePosition().x > _position.x && 
				pInputState->GetMousePosition().x < _position.x + _size.x && 
				pInputState->GetMousePosition().y > _position.y && 
				pInputState->GetMousePosition().y < _position.y + _size.y &&
				!_mouseIntercepted)
			{
				_mouseIntercepted = true;
				_state.IsHover = true;

				if (!_state.WasHover)
					Hover(eventArgs);

				if (pInputState->IsLeftButtonDown())
				{
					_state.IsActive = true;
					if (!pLastInputState->IsLeftButtonDown())
					{
						_state.IsFocus = true;
						_state.IsClicked = true;
						if (!_state.WasFocus)
							Focus(eventArgs);
					}
				}
				else
				{
					_state.IsActive = false;
				}
				if (pLastInputState->IsLeftButtonDown() && !pInputState->IsLeftButtonDown() && _state.IsClicked)
				{
					_state.IsClicked = false;
					_state.IsActive = false;
					Click(eventArgs);
				}
			}
			else
			{
				if (_state.WasHover)
				{
					_state.IsHover = false;
					Out(eventArgs);
				}

				_state.IsActive = false;
				if (pInputState->IsLeftButtonDown() && !pLastInputState->IsLeftButtonDown())
				{
					_state.IsFocus = false;
					if (_state.WasFocus)
						Blur(eventArgs);
				}
				if (!pInputState->IsLeftButtonDown())
				{
					_state.IsClicked = false;
				}
			}
		}
	}

	void UiElement::ResetMouseInterception()
	{
		_mouseIntercepted = false;
	}
}