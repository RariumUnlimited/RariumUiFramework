//License : Please see Rarium.LICENSE
#ifndef _RariumUiResourceType_

#define _RariumUiResourceType_

#include <Rarium/Core/Rarium.h>

namespace Rarium
{
	/// Type of resource used by a ui element
	enum class Rarium_API UiResourceType
	{
		Texture,
		UserShader,
		Text
	};
}

#endif