//License : Please see Rarium.LICENSE
#include <Rarium/Ui/UiDescriptor.h>

namespace Rarium
{
	UiDescriptor::UiDescriptor(std::string pName, std::string pPreviewTexturePath, Condition* pConditionPtr)
	{
		_name = pName;
		_previewTexturePath = pPreviewTexturePath;
		_conditionPtr = pConditionPtr;
		_displayMenu = true;
	}

	UiDescriptor::~UiDescriptor()
	{
		delete _conditionPtr;
	}
}