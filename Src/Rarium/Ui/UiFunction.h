//License : Please see Rarium.LICENSE
#ifndef _RariumUiFunction_

#define _RariumUiFunction_

#include <Rarium/Core/Rarium.h>
#include <string>
#include <Rarium/Input/InputState.h>

namespace Rarium
{
	/**
	*	Object that contains function related to ui manipulation
	*/
	class Rarium_API UiFunction
	{
	public:
		/**
		*	Request to change the currently displayed and active ui
		*	@param pUi Name of the new ui to display
		*/
		virtual void RequestUiChange(std::string pUi) = 0;
		/// Get the state of user input in the current frame
		virtual InputState* GetCurrentInputPtr() = 0;
		/// Get the state of user input in the last frame
		virtual InputState* GetLastInputPtr() = 0;
	};
}

#endif