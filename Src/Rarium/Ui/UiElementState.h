//License : Please see Rarium.LICENSE
#ifndef _RariumUiElementState_

#define _RariumUiElementState_

#include <Rarium/Core/Rarium.h>

namespace Rarium
{
	/**
	*	State of a ui element
	*/
	struct Rarium_API UiElementState
	{
	public:
		UiElementState()
		{
			WasClicked = false;
			IsClicked = false;
			WasHover = false;
			IsHover = false;
			WasFocus = false;
			IsFocus = false;
			WasActive = false;
			IsActive = false;
		}

		bool WasClicked;///< If the element was clicked last frame
		bool IsClicked;///< If the element is clicked in the current frame
		bool WasHover;///< If the mouse was hover the element in the last frame
		bool IsHover;///< If the mouse is hover the element in the current frame
		bool WasFocus;///< If the element was focused last frame
		bool IsFocus;///< If the element is focused in the current frame
		bool WasActive;///< If the element was active in the last frame (was being clicked on)
		bool IsActive;///< If the element is active in the current frame (is being clicked on)
	};
}

#endif