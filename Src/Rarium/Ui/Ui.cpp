//License : Please see Rarium.LICENSE
#include <Rarium/Ui/Ui.h>

namespace Rarium
{
	Ui::Ui()
	{
		
	}

	Ui::~Ui()
	{
		for (UiElement* elemPtr : _elements)
		{
			delete elemPtr;
		}
	}

	void Ui::Update(float pDelta, InputState* pState, InputState* pLastState)
	{
		for (int i = _elements.size() - 1; i >= 0; i--)
			_elements[i]->Update(pDelta,pState, pLastState);
	}
}