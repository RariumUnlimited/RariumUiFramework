//License : Please see Rarium.LICENSE
#ifndef _RariumUi_

#define _RariumUi_

#include <Rarium/Core/Rarium.h>
#include <Rarium/Ui/UiElement.h>
#include <Rarium/Ui/UIFunction.h>

namespace Rarium
{
	/**
	*	A User Interface to display on screen
	*/
	class Rarium_API Ui
	{
	public:
		/**
		*	Create the ui (on CPU)
		*/
		Ui();
		/**
		*	Destroy the ui
		*/
		~Ui();

		/// Get the list of all ui element that are in the ui
		inline std::vector<UiElement*> GetElements() { return _elements; }
		/// Set the object that can be used to call ui functions
		inline void SetUiFunction(UiFunction* pUiFunctionPtr) { _uiFunctionPtr = pUiFunctionPtr; }
		/// Get the pointer to the object that can be used to call ui functions
		inline UiFunction* GetUiFunctionPtr() { return _uiFunctionPtr; }

		/**
		*	Update the ui 
		*	@param pDelta Delta time since last update
		*	@param pState State of user input in current frame
		*	@param pLastState State of user input in last frame
		*/
		virtual void Update(float pDelta, InputState* pState, InputState* pLastState);

	protected:
		std::vector<UiElement*> _elements;///< List of all ui element that are in the ui
		UiFunction* _uiFunctionPtr;///< Pointer to the object that can be used to call ui functions
		
	};

}

#endif