//License : Please see Rarium.LICENSE
#ifndef _RariumUiElement_

#define _RariumUiElement_

#include <Rarium/Core/Rarium.h>
#include <Rarium/Ui/UiResourceType.h>
#include <Rarium/Render/RenderFunction.h>
#include <Rarium/Input/InputState.h>
#include <Rarium/Ui/UiElementState.h>
#include <Rarium/Core/Event.h>
#include <Rarium/Ui/MouseEventArg.h>

#include <string>
#include <vector>

namespace Rarium
{
	class Ui;

	/// Structure that store data about an element resource
	struct ResourceData { virtual ~ResourceData() {} };

	/// Data for texture resource
	struct TextureData : public ResourceData
	{
		std::string Path;///< Path to the texture
	};

	/// Data for text texture resource
	struct TextData : public ResourceData
	{
		std::string Text;///< Text to draw
		std::string Font;///< Path to the font to use
		unsigned int FontSize;///< Size of the text
		vec3 Color;///< Color of the text
	};

	/// Data for a shader resource
	struct ShaderData : public ResourceData
	{
		std::string SrcPath;///< Path to the shader file
	};

	/// A resource that is used for a ui element
	struct Rarium_API UiResource
	{
		/**
		*	Create a new resource
		*	@param pId Uniq id of the resource
		*	@param pType Type of the resource
		*	@param pRsData Pointer to the date of the resource, delete is done by the UiElement associated
		*	@param pIsDynamic True if the resource is dynamic and can be update after loading
		*/
		UiResource(std::string pId, UiResourceType pType, ResourceData* pRsData, bool pIsDynamic = false) { Id = pId; Type = pType; IsDynamic = pIsDynamic; RsData = pRsData; }

		std::string Id;///< Uniq id of the resource
		UiResourceType Type;///< Type of the resource
		bool IsDynamic;///< Pointer to the date of the resource, delete is done by the UiElement associated
		ResourceData* RsData;///< True if the resource is dynamic and can be update after loading
	};

	/**
	*	A ui element (commonly known as Widget), that can be drawn on screen as part of an ui
	*/
	class Rarium_API UiElement
	{
	public:
		/**
		*	Create a new UiElement
		*	@param pUi The ui that posses the element
		*	@param pPosition Position of the element on screen
		*	@param pParent An optionnal element that can be a parent of this element
		*	@param pIsDynamic If the element contains resources that can be dynamic
		*/
		UiElement(Ui* pUi, std::string pId, vec2 pPosition = vec2(0.0, 0.0), UiElement* pParent = nullptr, bool pIsDynamic = false);
		/**
		*	Destroy the ui element
		*/
		~UiElement();

		/// Get the id of the element
		inline std::string GetId() { return _id; }
		/// Get the list of resources used by the element
		inline std::vector<UiResource> GetResources() { return _resources; }
		/// Get the list of element children that this element have
		inline std::vector<UiElement*> GetChildren() { return _children; }
		/// Get the state of the element
		inline UiElementState GetStatePtr() { return _state; }
		/// Get the position on screen
		inline vec2 GetPosition() { return _position; }
		/// Get if the element is enabled/updatable
		inline bool IsEnabled() { return _isEnabled; }
		/// Set if the element is enabled/updatable
		inline void SetEnabled(bool pEnabled) { _isEnabled = pEnabled; }

		/// Move the element to a new position
		void MoveToPosition(vec2 pPosition);
		/// Move the element by an offset
		void MoveFromOffset(vec2 pOffset);

		/// Function that is called by the renderer when the resources of the element have been loaded on gpu
		virtual void UiElementLoaded(RenderFunction* pfunctionPtr) { _functionPtr = pfunctionPtr; }

		/// Check if the element require an update
		inline bool RequireUpdate() { return _requireUpdate; }
		/// Check if the element is dynamic
		inline bool IsDynamic() { return _isDynamic; }

		/**
		*	Update the element (and its state)
		*	@param pDelta Delta time since last update
		*/
		virtual void Update(float pDelta, InputState* pInputState, InputState* pLastInputState);
		/// Draw the element on screen using a RenderFunction object
		virtual void Draw() = 0;

		Event<void, MouseEventArg> Hover;///< Event that is triggered when the element is hover by the mouse
		Event<void, MouseEventArg> Out;///< Event that is triggered when the mouse leave the element
		Event<void, MouseEventArg> Focus;///< Event that is triggered when the element is focused
		Event<void, MouseEventArg> Blur;///< Event that is triggered when the element loses focus
		Event<void, MouseEventArg> Click;///< Event that is triggered when the element is clicked

		Event<void, vec2> Move;///< Event that is triggered when the element is moved

		static void ResetMouseInterception();

	protected:
		Ui* _ui;///< The ui that posses the element
		UiElement* _parent;///< An optionnal parent of this element
		std::vector<UiElement*> _children;///< List of children of the element
		std::vector<UiResource> _resources;///< List of resources used by the element
		std::string _id;///< Id of the element
		RenderFunction* _functionPtr;///< RenderFunction object that is used to call function to draw on screen
		vec2 _size;///< Size of the element
		vec2 _position;///< Position of the elment on screen
		UiElementState _state;///< Current state of the element

		bool _isEnabled;///< If yes or no the element is enabled/updatable

		bool _requireUpdate;///< If yes or no the element needs to be updated

	private:
		bool _isDynamic;///< If yes or no the element is dynamic

		static bool _mouseIntercepted;

	};
}

#endif