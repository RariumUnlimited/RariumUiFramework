//License : Please see Rarium.LICENSE
#ifndef _RariumMouseEventArg_

#define _RariumMouseEventArg_

#include <Rarium/Core/Rarium.h>
#include <glm/glm.hpp>

using namespace glm;

namespace Rarium
{
	/**
	*	Arguments given when a mouse event is triggered
	*/
	class Rarium_API MouseEventArg
	{
	public:
		/**
		*	Create a new MouseEventArg
		*	@param pMousePosition Position of mouse on screen at the time event
		*/
		MouseEventArg(vec2 pMousePosition) { _mousePosition = pMousePosition; }

		/// Get the position of mouse on screen a time of event
		inline vec2 GetMousePosition() { return _mousePosition; }

	protected:
		vec2 _mousePosition;///< Position of mouse on screen at the time of event
	};
}

#endif