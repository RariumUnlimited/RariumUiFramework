//License : Please see Rise.LICENSE
#ifndef _RiseRise_

#define _RiseRise_

#ifdef RISE_EXPORTS
	#ifdef WIN32
		#define Rise_API __declspec(dllexport) 
	#else
		#define Rise_API
	#endif
#else
	#ifdef WIN32
		#define Rise_API __declspec(dllimport) 
	#else
		#define Rise_API
	#endif
#endif

#endif