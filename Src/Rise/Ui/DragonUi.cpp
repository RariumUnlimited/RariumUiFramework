//License : Please see Rise.LICENSE
#include <Rise/Ui/DragonUi.h>
#include <Rarium/Core/Conditions/PropertyExistCondition.h>
#include <Rarium/Ui/UiElements/ImageElement.h>
#include <Rarium/Ui/UiElements/ButtonElement.h>
#include <Rarium/UserData/PropertyManager.h>
#include <Rarium/Core/Util.h>
using namespace Rarium;

namespace Rise
{
	DragonUiDescriptor::DragonUiDescriptor(std::string pName) : UiDescriptor(pName, "Rise/DragonUiPreview.dds", new PropertyExistCondition("Dragon", "Name"))
	{
		_displayMenu = true;
	}

	Ui* DragonUiDescriptor::GetUi()
	{
		return new DragonUi();
	}

	DragonUi::DragonUi()
	{
		_elements.push_back(new ImageElement(this, "Rise/Background.dds", vec2(0.0, 0.0)));
		_elements.push_back(new TextElement(this, "DragonName", vec2(100.0, 100.0), PropertyManager::GetRef().GetProperty<std::string>("Dragon", "Name").GetValue(), "Rise/redline.ttf", 50, vec3(1.0, 1.0, 1.0)));
		//_elements.push_back(new TextElement(this, "StatPoint", vec2(100.0, 80.0), PropertyManager::GetRef().GetProperty<std::string>("Dragon","StatPoint").GetValue(), "Rise/redline.ttf", 20, vec3(1.0, 1.0, 1.0)));

		_elements.push_back(new ImageElement(this, PropertyManager::GetRef().GetProperty<std::string>("Dragon", "Egg"), vec2(100.0, 160.0)));

		_elements.push_back(new ImageElement(this, "Rise/StatBackground.dds", vec2(875.0, 310.0)));

		_elements.push_back(new TextElement(this, "StatText", vec2(900.0, 630.0), "Soul Statistic", "Rise/redline.ttf", 50, vec3(1.0, 1.0, 1.0)));
		_elements.push_back(new TextElement(this, "Power", vec2(920.0, 580.0),std::string("Power : ") + PropertyManager::GetRef().GetProperty<std::string>("Dragon", "Power").GetValue(), "Rise/redline.ttf", 40, vec3(1.0, 1.0, 1.0)));
		_elements.push_back(new TextElement(this, "Will", vec2(920.0, 530.0), std::string("Will : ") + PropertyManager::GetRef().GetProperty<std::string>("Dragon", "Will").GetValue(), "Rise/redline.ttf", 40, vec3(1.0, 1.0, 1.0)));
		_elements.push_back(new TextElement(this, "Integrity", vec2(920.0, 480.0), std::string("Integrity : ") + PropertyManager::GetRef().GetProperty<std::string>("Dragon", "Integrity").GetValue(), "Rise/redline.ttf", 40, vec3(1.0, 1.0, 1.0)));
		_elements.push_back(new TextElement(this, "Purity", vec2(920.0, 430.0), std::string("Purity : ") + PropertyManager::GetRef().GetProperty<std::string>("Dragon", "Purity").GetValue(), "Rise/redline.ttf", 40, vec3(1.0, 1.0, 1.0)));
		_elements.push_back(new TextElement(this, "Haki", vec2(920.0, 380.0), std::string("Haki : ") + PropertyManager::GetRef().GetProperty<std::string>("Dragon", "Haki").GetValue(), "Rise/redline.ttf", 40, vec3(1.0, 1.0, 1.0)));
		_elements.push_back(new TextElement(this, "StatPoint", vec2(900.0, 330.0), std::string("Stat remaining : ") + PropertyManager::GetRef().GetProperty<std::string>("Dragon", "StatPoint").GetValue(), "Rise/redline.ttf", 20, vec3(1.0, 1.0, 1.0)));
	}
}