//License : Please see Rise.LICENSE
#ifndef _RiseNameUi_

#define _RiseNameUi_

#include <Rise/Core/Rise.h>
#include <Rarium/Ui/UiDescriptor.h>
#include <Rarium/Ui/Ui.h>
#include <Rarium/Ui/UiElements/TextEditElement.h>

namespace Rise
{
	class Rise_API NameUiDescriptor : public Rarium::UiDescriptor
	{
	public:
		NameUiDescriptor(std::string pName = "Name");
		virtual Rarium::Ui* GetUi() override;
	};

	class Rise_API NameUi : public Rarium::Ui
	{
	public:
		NameUi();

	protected:
		void ContinueButtonClick(Rarium::MouseEventArg pE);
		Rarium::TextEditElement* _textEditPtr;
	};
}

#endif