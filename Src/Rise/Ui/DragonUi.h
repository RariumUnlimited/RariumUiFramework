//License : Please see Rise.LICENSE
#ifndef _RiseDragonUi_

#define _RiseDragonUi_

#include <Rise/Core/Rise.h>
#include <Rarium/Ui/UiDescriptor.h>
#include <Rarium/Ui/Ui.h>

namespace Rise
{
	class Rise_API DragonUiDescriptor : public Rarium::UiDescriptor
	{
	public:
		DragonUiDescriptor(std::string pName = "Dragon");
		virtual Rarium::Ui* GetUi() override;
	};

	class Rise_API DragonUi : public Rarium::Ui
	{
	public:
		DragonUi();

	protected:

	};
}

#endif