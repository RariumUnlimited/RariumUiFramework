//License : Please see Rise.LICENSE
#include <Rise/Ui/NameUi.h>
#include <Rarium/Core/Conditions/PropertyNotExistCondition.h>
#include <Rarium/Ui/UiElements/ImageElement.h>
#include <Rarium/Ui/UiElements/ButtonElement.h>
#include <Rarium/UserData/PropertyManager.h>
#include <Rarium/Core/Util.h>
using namespace Rarium;

namespace Rise
{
	NameUiDescriptor::NameUiDescriptor(std::string pName) : UiDescriptor(pName, "Rise/NameUiPreview.dds", new PropertyNotExistCondition("Dragon", "Name"))
	{
		_displayMenu = false;
	}

	Ui* NameUiDescriptor::GetUi()
	{
		return new NameUi();
	}

	NameUi::NameUi()
	{
		_elements.push_back(new ImageElement(this, "Rise/Background.dds", vec2(0.0, 0.0)));
		_elements.push_back(new TextElement(this, "EnterName", vec2(350.0, 540.0), "Enter your dragon name", "Rise/redline.ttf", 50, vec3(1.0, 1.0, 1.0)));
		_textEditPtr = new TextEditElement(this, "Name", vec2(490.0, 400.0), "Rise/TextArea.dds", "Rise/TextAreaLight.dds", "Rise/redline.ttf", 30, vec3(1.0, 1.0, 1.0));
		_elements.push_back(_textEditPtr);

		ButtonElement* buttonPtr = new ButtonElement(this, "Continue", vec2(540, 300), "Rise/Button.dds", "Rise/ButtonDark.dds", "Rise/ButtonLight.dds", "Continue", "Rise/redline.ttf", 30, vec3(1.0, 1.0, 1.0));
		buttonPtr->Click += new Event<void, MouseEventArg>::T<NameUi>(this, &NameUi::ContinueButtonClick);
		_elements.push_back(buttonPtr);
	}

	void NameUi::ContinueButtonClick(Rarium::MouseEventArg pE)
	{
		std::string name = _textEditPtr->GetRealText();
		if (name.size() >= 4)
		{
			PropertyManager::GetRef().UpdateProperty(Property<std::string>("Dragon", "Name", name));

			unsigned int temp = unsigned int(name[0]);
			temp = temp | ((unsigned int)(name[1]) << 8);
			temp = temp | ((unsigned int)(name[2]) << 16);
			temp = temp | ((unsigned int)(name[3]) << 24);
			temp = temp % 6 + 1;

			PropertyManager::GetRef().UpdateProperty(Property<std::string>("Dragon", "Egg", std::string("Rise/Egg"+ToString(temp)+".dds")));
			PropertyManager::GetRef().UpdateProperty(Property<int>("Dragon", "Power", 5));
			PropertyManager::GetRef().UpdateProperty(Property<int>("Dragon", "Will", 5));
			PropertyManager::GetRef().UpdateProperty(Property<int>("Dragon", "Integrity", 5));
			PropertyManager::GetRef().UpdateProperty(Property<int>("Dragon", "Purity", 5));
			PropertyManager::GetRef().UpdateProperty(Property<int>("Dragon", "Haki", 5));
			PropertyManager::GetRef().UpdateProperty(Property<int>("Dragon", "StatPoint", 0));

			_uiFunctionPtr->RequestUiChange("Dragon");
		}
	}
}