//License : Please see Rise.LICENSE
#include <Rise/Core/Rise.h>

#include <vector>
#include <string>
#include <Rarium/Ui/Ui.h>
#include <Rise/Ui/NameUi.h>
#include <Rise/Ui/DragonUi.h>
#include <Rarium/UserData/PropertyManager.h>
#include <functional>


extern "C" {
	Rise_API std::vector<std::string>* GetPluginFeatures()
	{
		std::vector<std::string>* features = new std::vector<std::string>;
		features->push_back("StartupUi");
		features->push_back("Uis");
		return features;
	}

	Rise_API std::vector<Rarium::UiDescriptor*>* GetStartupUi()
	{
		std::vector<Rarium::UiDescriptor*>* result = new std::vector<Rarium::UiDescriptor*>;
		if (Rarium::PropertyManager::GetRef().PropertyExists("Dragon", "Name"))
			result->push_back(new Rise::DragonUiDescriptor());
		else
			result->push_back(new Rise::NameUiDescriptor());
		
		return result;
	}

	Rise_API std::vector<Rarium::UiDescriptor*>* GetUis()
	{
		std::vector<Rarium::UiDescriptor*>* result = new std::vector<Rarium::UiDescriptor*>;
		if (!Rarium::PropertyManager::GetRef().PropertyExists("Dragon", "Name"))
			result->push_back(new Rise::DragonUiDescriptor());

		return result;
	}
}