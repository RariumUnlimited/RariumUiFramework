﻿//------------------------------------------------------------------------------
// <copyright file="PackageCommand.cs" company="Company">
//     Copyright (c) Company.  All rights reserved.
// </copyright>
//------------------------------------------------------------------------------

using System;
using System.ComponentModel.Design;
using System.Globalization;
using Microsoft.VisualStudio.Shell;
using Microsoft.VisualStudio.Shell.Interop;
using System.Diagnostics;
using System.IO;
using System.Collections.Generic;
using System.IO.Compression;
using System.Linq;

namespace PackageRariumApp
{
    /// <summary>
    /// Command handler
    /// </summary>
    internal sealed class PackageCommand
    {
        /// <summary>
        /// Command ID.
        /// </summary>
        public const int CommandId = 0x0100;

        /// <summary>
        /// Command menu group (command set GUID).
        /// </summary>
        public static readonly Guid CommandSet = new Guid("4bdef4cd-a5a5-4b0f-9270-5761ccd88c42");

        /// <summary>
        /// VS Package that provides this command, not null.
        /// </summary>
        private readonly Package package;

        /// <summary>
        /// Initializes a new instance of the <see cref="PackageCommand"/> class.
        /// Adds our command handlers for menu (commands must exist in the command table file)
        /// </summary>
        /// <param name="package">Owner package, not null.</param>
        private PackageCommand(Package package)
        {
            if (package == null)
            {
                throw new ArgumentNullException("package");
            }

            this.package = package;

            OleMenuCommandService commandService = this.ServiceProvider.GetService(typeof(IMenuCommandService)) as OleMenuCommandService;
            if (commandService != null)
            {
                var menuCommandID = new CommandID(CommandSet, CommandId);
                var menuItem = new MenuCommand(this.PackageRariumApp, menuCommandID);
                commandService.AddCommand(menuItem);
            }
        }

        private void PackageRariumApp(object sender, EventArgs e)
        {
            EnvDTE80.DTE2 dte = (EnvDTE80.DTE2)ServiceProvider.GetService(typeof(EnvDTE.DTE));

            if (dte.Solution.IsOpen)
            {
                OutputInBuild("Building a rarium app package.");
                bool binaryFound = false;
                bool rariumFound = false;
                EnvDTE.Project binaryProject = null;
                string exeName = "";
                foreach (EnvDTE.Project proj in dte.Solution.Projects)
                {
                    if (proj.Name == "Binary")
                    {
                        binaryFound = true;
                        binaryProject = proj;
                    }
                    else if (proj.Name == "Rarium")
                    {
                        rariumFound = true;
                    }
                }

                if (binaryFound && rariumFound)
                {
                    EnvDTE.SolutionBuild slnBuild = dte.Solution.SolutionBuild;

                    foreach (EnvDTE.SolutionConfiguration config in slnBuild.SolutionConfigurations)
                    {
                        if (config.Name == "Release")
                            config.Activate();
                    }
                    OutputInBuild("Building Solution.");
                    slnBuild.Build(true);
                    OutputInBuild("Done Building Solution.");


                    FileInfo slnFile = new FileInfo(dte.Solution.FullName);
                    DirectoryInfo slnDirectory = slnFile.Directory;


                    DirectoryInfo distDir = new DirectoryInfo(slnDirectory.FullName + "\\Dist\\");

                    string targetName = "";

                    if (distDir.Exists)
                    {
                        List<FileInfo> filesRoot = new List<FileInfo>(distDir.GetFiles());
                        FileInfo exeFile = (from f in filesRoot
                                            where !f.Name.EndsWith("Debug.exe") && f.Extension == ".exe"
                                            select f).First();
                        IEnumerable<FileInfo> dllFiles = (from f in filesRoot
                                                          where !f.Name.EndsWith("Debug.dll") && !f.Name.EndsWith("d.dll") && f.Extension == ".dll"
                                                          select f);

                        targetName = exeFile.Name.Substring(0, (int)(exeFile.Name.Length) - 4);


                        string archivePath = slnDirectory.FullName + "\\" + targetName + ".zip";

                        FileInfo fArchive = new FileInfo(archivePath);
                        if (fArchive.Exists)
                            File.Delete(fArchive.FullName);


                        using (ZipArchive archive = ZipFile.Open(archivePath, ZipArchiveMode.Update))
                        {
                            archive.CreateEntryFromFile(exeFile.FullName, targetName + "/" + exeFile.Name, CompressionLevel.Optimal);
                            foreach (FileInfo file in dllFiles)
                                archive.CreateEntryFromFile(file.FullName, targetName + "/" + file.Name, CompressionLevel.Optimal);


                            DirectoryInfo pluginsDir = new DirectoryInfo(slnDirectory.FullName + "\\Dist\\Plugins\\");
                            if (pluginsDir.Exists)
                            {
                                List<FileInfo> filesPlugins = new List<FileInfo>(pluginsDir.GetFiles());
                                IEnumerable<FileInfo> pluginsDlls = (from f in filesPlugins
                                                                     where f.Extension == ".dll"
                                                                     select f);
                                foreach (FileInfo file in pluginsDlls)
                                    archive.CreateEntryFromFile(file.FullName, targetName + "/Plugins/" + file.Name, CompressionLevel.Optimal);
                            }
                            else
                                OutputInBuild("Error, Plugins directory not found.");


                            DirectoryInfo assetsDir = new DirectoryInfo(slnDirectory.FullName + "\\Dist\\Assets\\");
                            if (assetsDir.Exists)
                            {
                                ProcessDir(slnDirectory.FullName + "\\Dist\\", "Assets", archive, targetName);
                            }
                            else
                                OutputInBuild("Error, Assets directory not found.");
                        }

                        OutputInBuild("Done building rarium app package. " + targetName + ".zip package has been created.");
                    }
                    else
                        OutputInBuild("Error, Dist directory not found.");
                }
                else
                {
                    OutputInBuild("Error, Binary or Rarium projects not found.");
                }
            }
        }

        void ProcessDir(string distDir, string currentDir, ZipArchive archive, string targetName)
        {
            DirectoryInfo dir = new DirectoryInfo(distDir + currentDir + "\\");
            foreach(FileInfo f in dir.GetFiles())
            {
                archive.CreateEntryFromFile(f.FullName, targetName+"/" +currentDir+"/" + f.Name, CompressionLevel.Optimal);
            }
            foreach (DirectoryInfo d in dir.GetDirectories())
            {
                ProcessDir(distDir, currentDir + "/" + d.Name, archive, targetName);
            }
        }

        void OutputInBuild(string buildMessage)
        {
            EnvDTE80.DTE2 dte = (EnvDTE80.DTE2)ServiceProvider.GetService(typeof(EnvDTE.DTE));

            EnvDTE.OutputWindowPanes panes =
                dte.ToolWindows.OutputWindow.OutputWindowPanes;
            foreach (EnvDTE.OutputWindowPane pane in panes)
            {
                if (pane.Name.Contains("Build"))
                {
                    pane.OutputString(buildMessage + "\n");
                    pane.Activate();
                    return;
                }
            }
        }

        /// <summary>
        /// Gets the instance of the command.
        /// </summary>
        public static PackageCommand Instance
        {
            get;
            private set;
        }

        /// <summary>
        /// Gets the service provider from the owner package.
        /// </summary>
        private IServiceProvider ServiceProvider
        {
            get
            {
                return this.package;
            }
        }

        /// <summary>
        /// Initializes the singleton instance of the command.
        /// </summary>
        /// <param name="package">Owner package, not null.</param>
        public static void Initialize(Package package)
        {
            Instance = new PackageCommand(package);
        }

        private void StartNotepad(object sender, EventArgs e)
        {
            Console.WriteLine("Launching Notepad");

            Process proc = new Process();
            proc.StartInfo.FileName = "notepad.exe";
            proc.Start();
        }
    }
}
