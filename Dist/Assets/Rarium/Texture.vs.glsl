#version 450 core

layout(location = 0) uniform vec4 PositionSize;

layout(std140, binding = 0) uniform GlobalDataBlock
{
	vec2 ScreenResolution;
} GlobalData;

out VsOutBlock
{
	vec2 TexCoord;
} VsOut;

void main()
{
	vec2 tempPos = PositionSize.xy;
	vec2 sizeTemp = PositionSize.zw + PositionSize.xy;

	tempPos *= 2;
	tempPos -= GlobalData.ScreenResolution;
	tempPos /= GlobalData.ScreenResolution;


	sizeTemp *= 2;
	sizeTemp -= GlobalData.ScreenResolution;
	sizeTemp /= GlobalData.ScreenResolution;

	vec4 vertices[4] = vec4[4](
		vec4(tempPos.x,		tempPos.y,	1.0, 1.0),
		vec4(sizeTemp.x,	tempPos.y,	1.0, 1.0),
		vec4(tempPos.x,		sizeTemp.y, 1.0, 1.0),
		vec4(sizeTemp.x,	sizeTemp.y, 1.0, 1.0));
	
	vec2 tc[4] = vec2[4](
		vec2(0.0,0.0),
		vec2(1.0,0.0),
		vec2(0.0,1.0),
		vec2(1.0,1.0));

	vec4 pos = vertices[gl_VertexID];

	VsOut.TexCoord = tc[gl_VertexID];

    gl_Position = pos;
}
