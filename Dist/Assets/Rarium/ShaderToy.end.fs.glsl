

void main()
{
	vec4 fragColor; 
	vec2 fragCoord = gl_FragCoord.xy - PositionSize.xy;

	iChannel0 = sampler2D(Texture0);
	iChannel1 = sampler2D(Texture1);
	iChannel2 = sampler2D(Texture2);
	iChannel3 = sampler2D(Texture3);

	ivec2 channelSizeTemp0 = textureSize(iChannel0,0);
	ivec2 channelSizeTemp1 = textureSize(iChannel1,0);
	ivec2 channelSizeTemp2 = textureSize(iChannel2,0);
	ivec2 channelSizeTemp3 = textureSize(iChannel3,0);

	iChannelResolution[0] = vec3(int(channelSizeTemp0.x),int(channelSizeTemp0.y),1.0);
	iChannelResolution[1] = vec3(int(channelSizeTemp1.x),int(channelSizeTemp1.y),1.0);
	iChannelResolution[2] = vec3(int(channelSizeTemp2.x),int(channelSizeTemp2.y),1.0);
	iChannelResolution[3] = vec3(int(channelSizeTemp3.x),int(channelSizeTemp3.y),1.0);

	iResolution = vec3(PositionSize.zw,1.0);

	mainImage(fragColor,fragCoord);
	colorOutput = fragColor;
}
