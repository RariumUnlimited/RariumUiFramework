#version 450 core
#extension GL_NV_gpu_shader5 : require
#extension GL_ARB_bindless_texture : require

layout(std140, binding = 0) uniform GlobalDataBlock
{
	vec2 ScreenResolution;
} GlobalData;

in VsOutBlock
{
	vec2 TexCoord;
} FsIn;

out vec4 colorOutput;

uniform uint64_t Texture0;
uniform uint64_t Texture1;
uniform uint64_t Texture2;
uniform uint64_t Texture3;
uniform vec4      iMouse;
uniform float     iGlobalTime;
uniform vec4 PositionSize;

vec3      iChannelResolution[4];
sampler2D iChannel0;
sampler2D iChannel1;
sampler2D iChannel2;
sampler2D iChannel3;
vec3      iResolution;

