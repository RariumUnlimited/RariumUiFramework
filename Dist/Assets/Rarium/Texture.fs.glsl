#version 450 core
#extension GL_NV_gpu_shader5 : require
#extension GL_ARB_bindless_texture : require

layout(location = 1) uniform uint64_t Texture;

out vec4 color;


in VsOutBlock
{
	vec2 TexCoord;
} FsIn;

void main()
{
	color = texture(sampler2D(Texture), FsIn.TexCoord);
}
