## libRarium.so
Rarium is an c++ User Interface framework.
Rise, an example project, is provided as a showcase of the framework features.

## Features

- Widgets : buttons, images, static and dynamic texts, line edition, ShaderToy widget
- Rendering using OpenGL 4.5 Core
- Plugin system
- User data management

## Developing and building application 

You need Visual Studio 2015. All external libraries are provided, headers are in Extern and libraries in Lib.

## Deploying an application

You can package an application using the PackageRariumApp Visual Studio extension. It will create a zip file that you can distribute. Client will need to install the Visual Studio 2015 redist : [Get the redist](https://www.microsoft.com/fr-FR/download/details.aspx?id=48145)

## License

All Rarium and Rise source code and assets are provided under the MIT license (See LICENSE file)